//
//  LinkBuilder.h
//  MyReddit
//
//  Created by Andrej Trajkovski on 21.12.15.
//  Copyright © 2015 Andrej Trajkovski. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Link.h"

enum {
    LinkBuilderInvalidJSONError = 0,
    LinkBuilderMissingDataError
};

@interface LinkBuilder : NSObject

- (NSArray *)linksFromJSON: (NSString *)objectNotation
                     error: (NSError **)error;
@end
