//
//  Link.h
//  MyReddit
//
//  Created by Andrej Trajkovski on 11.10.15.
//  Copyright © 2015 г. Andrej Trajkovski. All rights reserved.
//

#import "Post.h"

@interface Link : Post

@property (strong,  nonatomic) NSString *domain;
@property (strong,  nonatomic) NSString *urlString;
@property (strong,  nonatomic) NSURL *thumbnail;

@end
