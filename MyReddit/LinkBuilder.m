//
//  LinkBuilder.m
//  MyReddit
//
//  Created by Andrej Trajkovski on 21.12.15.
//  Copyright © 2015 Andrej Trajkovski. All rights reserved.
//

#import "LinkBuilder.h"

@implementation LinkBuilder
//- (NSArray *)linksFromJSON: (NSString *)objectNotation
//                     error: (NSError **)error{
//    NSParameterAssert(objectNotation != nil);
//    if (error != NULL) {
//        *error = [NSError errorWithDomain:LinkBuilderInvalidJSONError
//                                     code:LinkBuilderErrorDomain userInfo: nil];
//    }
//    return nil;
//}
//
- (NSArray *)linksFromJSON:(NSString *)objectNotation
                         error:(NSError **)error {
    NSParameterAssert(objectNotation != nil);
    NSData *unicodeNotation = [objectNotation
                               dataUsingEncoding: NSUTF8StringEncoding];
    NSError *localError = nil;
    id jsonObject = [NSJSONSerialization
                     JSONObjectWithData: unicodeNotation options: 0
                     error: &localError];
    NSDictionary *parsedObject = (id)jsonObject;
    if (parsedObject == nil) {
        if (error != NULL) {
            *error = [NSError errorWithDomain: LinkBuilderErrorDomain
                                         code: LinkBuilderInvalidJSONError userInfo: nil];
        }
        return nil;
    }
    NSString *kind = [parsedObject objectForKey: @"kind"];
    if (kind == nil || ![kind isEqualToString:@"Listing"]) {
        if (error != NULL) {
            *error = [NSError errorWithDomain: LinkBuilderErrorDomain
                                         code: LinkBuilderMissingDataError userInfo:nil];
        }
        return nil;
    }
    
    NSDictionary *data = [parsedObject objectForKey: @"data"];
    if (data == nil || ![data isKindOfClass:[NSDictionary class]]) {
        if (error != NULL) {
            *error = [NSError errorWithDomain: LinkBuilderErrorDomain
                                         code: LinkBuilderMissingDataError userInfo:nil];
        }
        return nil;
    }
    
    NSArray *childrenObjects = [data objectForKey: @"children"];
    
    NSMutableArray *links = [NSMutableArray array];
    for (NSDictionary *linkObject in childrenObjects)
    {
        Link *link = [[Link alloc] init];
        
        NSDictionary *linkObjectData = [linkObject valueForKey:@"data"];
        
        link.identifier = [linkObjectData valueForKey:@"id"];
        link.score = [NSNumber numberWithInteger:[[linkObjectData valueForKey:@"score"]integerValue]];
        link.title = [linkObjectData valueForKey:@"title"];
        link.domain = [linkObjectData valueForKey:@"domain"];
        link.urlString = [linkObjectData valueForKey:@"url"];

        [links addObject:link];
    }
    
    return links;

}

NSString *LinkBuilderErrorDomain = @"LinkBuilderErrorDomain";

@end
