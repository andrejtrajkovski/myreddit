//
//  main.m
//  MyReddit
//
//  Created by Andrej Trajkovski on 02.9.15.
//  Copyright (c) 2015 г. Andrej Trajkovski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
