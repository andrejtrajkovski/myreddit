//
//  MyRedditObjectConfiguration.h
//  MyReddit
//
//  Created by Andrej Trajkovski on 28.11.17.
//  Copyright © 2017 Andrej Trajkovski. All rights reserved.
//

#import <Foundation/Foundation.h>

@class AppManager;
@interface MyRedditObjectConfiguration : NSObject

-(AppManager *)appManager;

@end
