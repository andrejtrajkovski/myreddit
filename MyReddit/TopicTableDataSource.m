//
//  TopicTableDataSource.m
//  MyReddit
//
//  Created by Andrej Trajkovski on 20.10.17.
//  Copyright © 2017 Andrej Trajkovski. All rights reserved.
//

#import "TopicTableDataSource.h"
#import "Topic.h"

@implementation TopicTableDataSource{
    
    NSArray *topics;
}

-(void)setTopics:(NSArray *)newTopics{
    
    topics = newTopics;
}

#pragma mark - Table View Data Source

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    NSParameterAssert(section == 0);
    return [topics count];
}

NSString *topicCellReuseIdentifier = @"Topic";

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSParameterAssert([indexPath section] == 0);
    NSParameterAssert([indexPath row] < [topics count]);
    UITableViewCell *topicCell = [tableView dequeueReusableCellWithIdentifier: topicCellReuseIdentifier];
    if (!topicCell) {
        topicCell = [[UITableViewCell alloc] initWithStyle: UITableViewCellStyleDefault reuseIdentifier: topicCellReuseIdentifier];
    }
    topicCell.textLabel.text = [[self topicForIndexPath:indexPath] name];
    
    return topicCell;
}

-(Topic *)topicForIndexPath:(NSIndexPath *)indexPath {
    return [topics objectAtIndex: [indexPath row]];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    Topic *selectedTopic = [self topicForIndexPath:indexPath];
    NSNotification *selectedTopicNotification = [NSNotification notificationWithName:TopicTableDidSelectTopicNotification object:selectedTopic];
    [[NSNotificationCenter defaultCenter] postNotification:selectedTopicNotification];
}

@end

NSString *TopicTableDidSelectTopicNotification = @"TopicTableDidSelectTopicNotification";
