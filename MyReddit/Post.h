//
//  Post.h
//  MyReddit
//
//  Created by Andrej Trajkovski on 06.10.15.
//  Copyright © 2015 г. Andrej Trajkovski. All rights reserved.
//

#import <Foundation/Foundation.h>
@class Comment;

typedef NS_ENUM(NSInteger, PostType) {
    PostTypeComment,
    PostTypeLink,
};

@interface Post : NSObject

@property (strong,  nonatomic) NSArray *comments;
@property (strong,  nonatomic) NSString *title;
@property (strong,  nonatomic) NSNumber *score;
@property (assign,  nonatomic) PostType postType;
@property (strong,  nonatomic) NSString *identifier;
@property (strong,  nonatomic) NSString *author;

-(void)addChildComment:(Comment *)childComment;

@end
