//
//  WebViewViewController.h
//  MyReddit
//
//  Created by Andrej Trajkovski on 20.12.17.
//  Copyright © 2017 Andrej Trajkovski. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Link;

@interface WebViewViewController : UIViewController

@property (strong, nonatomic) Link *link;

@end
