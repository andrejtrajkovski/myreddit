//
//  AppDelegate.h
//  MyReddit
//
//  Created by Andrej Trajkovski on 02.9.15.
//  Copyright (c) 2015 г. Andrej Trajkovski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

