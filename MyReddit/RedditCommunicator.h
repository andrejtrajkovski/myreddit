//
//  RedditCommunicator.h
//  MyReddit
//
//  Created by Andrej Trajkovski on 09.12.15.
//  Copyright © 2015 Andrej Trajkovski. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Topic.h"
#import "AFNetworking.h"
#import "Link.h"
#import "RedditCommunicatorDelegate.h"

@interface RedditCommunicator : NSObject <NSURLConnectionDataDelegate>{
    
@protected
    NSURL *fetchURL;
    NSURLConnection *fetchingConnection;
@private
    void (^errorHandler)(NSError *);
    void (^successHandler)(NSString *);
    id <RedditCommunicatorDelegate> __weak delegate;
}


-(void)searchForLinksOnTopic:(Topic *)topic;
-(void)searchForCommentsOnLink:(Link *)link;

@property (weak, nonatomic) id <RedditCommunicatorDelegate> delegate;

@property (strong, nonatomic) NSMutableData *receivedData;

@end

extern NSString *RedditCommunicatorErrorDomain;
