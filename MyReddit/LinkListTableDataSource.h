//
//  PostListTableDataSource.h
//  MyReddit
//
//  Created by Andrej Trajkovski on 24.11.17.
//  Copyright © 2017 Andrej Trajkovski. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Topic;
@interface LinkListTableDataSource : NSObject <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) Topic *topic;

@end

extern NSString *placeholderCellReuseIdentifier;
extern NSString *LinkTableDidSelectLinkNotification;
extern NSString *LinkTableDidSelectCommentsNotification;
