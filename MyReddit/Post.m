//
//  Post.m
//  MyReddit
//
//  Created by Andrej Trajkovski on 06.10.15.
//  Copyright © 2015 г. Andrej Trajkovski. All rights reserved.
//

#import "Post.h"
#import "Comment.h"

@implementation Post

-(void)addChildComment:(Comment *)childComment{
    
    if (!self.comments) {
        self.comments = [NSArray new];
    }
    self.comments = [self.comments arrayByAddingObject:childComment];
}

@end
