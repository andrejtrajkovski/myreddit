//
//  CommentBuilder.m
//  MyReddit
//
//  Created by Andrej Trajkovski on 29.8.17.
//  Copyright © 2017 Andrej Trajkovski. All rights reserved.
//

#import "CommentBuilder.h"
#import "Comment.h"
#import "Link.h"

@implementation CommentBuilder

- (void)addCommentsToLink: (Link *)link fromJSON: (NSString *)objectNotation error: (NSError **)error{
    NSParameterAssert(objectNotation != nil);
    NSParameterAssert(link != nil);
    NSData *unicodeNotation = [objectNotation dataUsingEncoding: NSUTF8StringEncoding];
    NSError *localError = nil;
    NSArray *commentData = [NSJSONSerialization JSONObjectWithData: unicodeNotation options: 0  error: &localError];
    if (commentData == nil) {
        if (error) {
            NSMutableDictionary *userInfo = [NSMutableDictionary dictionaryWithCapacity: 1];
            if (localError != nil) {
                [userInfo setObject: localError forKey: NSUnderlyingErrorKey];
            }
            *error = [NSError errorWithDomain: CommentBuilderErrorDomain code: CommentBuilderInvalidJSONError userInfo: userInfo];
        }
    }
    
//    NSString *linkInJsonId = [commentData objectAtIndex:1][@"children"][0][@"data"][@"id"];
//
//    if (![linkInJsonId isEqualToString:link.identifier]) {
//
//        if (error) {
//            *error = [NSError errorWithDomain: CommentBuilderErrorDomain code: CommentBuilderWrongLinkError userInfo: nil];
//        }
//
//    }
    
    NSArray *comments = [commentData objectAtIndex:1][@"data"][@"children"];
    if (comments == nil) {
        if (error) {
            *error = [NSError errorWithDomain: CommentBuilderErrorDomain code:CommentBuilderInvalidJSONError userInfo: nil];
        }
    }
    
    [self addRepliesToPost:link fromReplies:comments];
}

-(void)addRepliesToPost:(Post *)post fromReplies:(NSArray *)replies {
    
    for (NSDictionary *repliesAllData in replies) {
        
        NSDictionary *replyData = repliesAllData[@"data"];
        
        Comment *thisComment = [[Comment alloc] init];
        thisComment.identifier = [replyData valueForKey:@"id"];
        thisComment.score = [NSNumber numberWithInteger:[[replyData valueForKey:@"score"]integerValue]];
        thisComment.title = [replyData valueForKey:@"body"];
        thisComment.fatherId = post.identifier;
        thisComment.depth = [replyData[@"depth"] integerValue];
        NSDictionary *subReplies = replyData[@"replies"];
        if (subReplies && [subReplies isKindOfClass:[NSDictionary class]]) {
            NSArray *subComments = subReplies[@"data"][@"children"];
            if (subComments) {
                if ([subComments isKindOfClass:[NSArray class]]&& subReplies.count > 0) {
                    [self addRepliesToPost:thisComment fromReplies:subComments];
                }
            }
        }
        [post addChildComment:thisComment];
    }
}

//-(NSArray *)commentsFromJSON:(NSString *)objectNotation
//                       error:(NSError *__autoreleasing *)error{
//    
//    NSParameterAssert(objectNotation != nil);
//    NSData *unicodeNotation = [objectNotation
//                               dataUsingEncoding: NSUTF8StringEncoding];
//    NSError *localError = nil;
//    id jsonObject = [NSJSONSerialization
//                     JSONObjectWithData: unicodeNotation options: 0
//                     error: &localError];
//    NSDictionary *parsedObject = (id)jsonObject;
//    if (parsedObject == nil) {
//        if (error != NULL) {
//            *error = [NSError errorWithDomain: CommentBuilderErrorDomain
//                                         code: CommentBuilderInvalidJSONError userInfo: nil];
//        }
//        return nil;
//    }
//    NSString *kind = [parsedObject objectForKey: @"kind"];
//    if (kind == nil || ![kind isEqualToString:@"Listing"]) {
//        if (error != NULL) {
//            *error = [NSError errorWithDomain: CommentBuilderErrorDomain
//                                         code: CommentBuilderMissingDataError userInfo:nil];
//        }
//        return nil;
//    }
//    
//    NSDictionary *data = [parsedObject objectForKey: @"data"];
//    if (data == nil || ![data isKindOfClass:[NSDictionary class]]) {
//        if (error != NULL) {
//            *error = [NSError errorWithDomain: CommentBuilderErrorDomain
//                                         code: CommentBuilderMissingDataError userInfo:nil];
//        }
//        return nil;
//    }
//    
//    NSArray *childrenObjects = [data objectForKey: @"children"];
//    
//    NSMutableArray *comments = [NSMutableArray array];
//    for (NSDictionary *commentObject in childrenObjects)
//    {
//        Comment *comment = [[Comment alloc] init];
//        
//        NSDictionary *linkObjectData = [commentObject valueForKey:@"data"];
//        
//        comment.identifier = [linkObjectData valueForKey:@"id"];
//        comment.score = [NSNumber numberWithInteger:[[linkObjectData valueForKey:@"score"]integerValue]];
//        comment.title = [linkObjectData valueForKey:@"title"];
//        
//        [comments addObject:comment];
//    }
//    
//    return comments;
//}

NSString *CommentBuilderErrorDomain = @"CommentBuilderErrorDomain";

@end
