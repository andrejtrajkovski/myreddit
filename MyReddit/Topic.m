//
//  Topic.m
//  MyReddit
//
//  Created by Andrej Trajkovski on 08.9.15.
//  Copyright (c) 2015 г. Andrej Trajkovski. All rights reserved.
//

#import "Topic.h"

@implementation Topic

@synthesize name, links;

-(id)initWithName:(NSString *)aName{

    if(self = [super init]){
        name = aName;
    }
    return self;
}

-(void)addLink:(Link *)link{
    
    if (!links) {
        links = [NSArray new];
    }
    NSArray *newLinks = [links arrayByAddingObject: link];
    links = newLinks;
}

@end
