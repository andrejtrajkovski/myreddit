//
//  UITableViewCell+Utility.h
//  MyReddit
//
//  Created by Andrej Trajkovski on 26.11.17.
//  Copyright © 2017 Andrej Trajkovski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableViewCell (Utility)

+ (UINib *)nib;
+ (NSString *)cellReuseIdentifier;

@end
