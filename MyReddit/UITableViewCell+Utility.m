//
//  UITableViewCell+Utility.m
//  MyReddit
//
//  Created by Andrej Trajkovski on 26.11.17.
//  Copyright © 2017 Andrej Trajkovski. All rights reserved.
//

#import "UITableViewCell+Utility.h"

@implementation UITableViewCell (Utility)

+ (UINib *)nib
{
    return [UINib nibWithNibName:NSStringFromClass([self class]) bundle:[NSBundle bundleForClass:[self class]]];
}

+ (NSString *)cellReuseIdentifier
{
    return NSStringFromClass([self class]);
}

@end
