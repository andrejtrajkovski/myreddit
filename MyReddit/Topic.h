//
//  Topic.h
//  MyReddit
//
//  Created by Andrej Trajkovski on 08.9.15.
//  Copyright (c) 2015 г. Andrej Trajkovski. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Link;
@interface Topic : NSObject

-(id)initWithName:(NSString *)aName;

@property (readonly) NSString *name;

@property (strong,  nonatomic) NSArray *links;

-(void)addLink:(Link *)link;

@end
