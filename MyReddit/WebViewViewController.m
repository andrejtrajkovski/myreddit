//
//  WebViewViewController.m
//  MyReddit
//
//  Created by Andrej Trajkovski on 20.12.17.
//  Copyright © 2017 Andrej Trajkovski. All rights reserved.
//

#import "WebViewViewController.h"
#import "Link.h"

@interface WebViewViewController ()

@end

@implementation WebViewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UIWebView *ww = [UIWebView new];
    [ww setFrame:self.view.frame];
    [self.view addSubview:ww];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:self.link.urlString]];
    [ww loadRequest:urlRequest];
}

@end
