//
//  RedditCommunicatorDelegate.h
//  MyReddit
//
//  Created by Andrej Trajkovski on 15.9.17.
//  Copyright © 2017 Andrej Trajkovski. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol RedditCommunicatorDelegate <NSObject>

- (void)searchingForLinksFailedWithError: (NSError *)error;

- (void)searchingForCommentsFailedWithError: (NSError *)error;

- (void)receivedLinksJSON: (NSString *)objectNotation;

- (void)receivedCommentsJSON: (NSString *)objectNotation;

@end
