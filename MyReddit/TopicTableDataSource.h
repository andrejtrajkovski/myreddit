//
//  TopicTableDataSource.h
//  MyReddit
//
//  Created by Andrej Trajkovski on 20.10.17.
//  Copyright © 2017 Andrej Trajkovski. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Topic;

@interface TopicTableDataSource : NSObject <UITableViewDataSource, UITableViewDelegate>

-(void)setTopics:(NSArray *)newTopics;
-(Topic *)topicForIndexPath:(NSIndexPath *)indexPath;
    
@end

extern NSString *TopicTableDidSelectTopicNotification;
