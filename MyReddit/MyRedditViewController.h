//
//  MyRedditViewController.h
//  MyReddit
//
//  Created by Andrej Trajkovski on 27.9.17.
//  Copyright © 2017 Andrej Trajkovski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppManagerDelegate.h"

@class MyRedditObjectConfiguration, AppManager;

@interface MyRedditViewController : UIViewController <AppManagerDelegate>

@property (weak) IBOutlet UITableView *tableView;
@property (strong) id <UITableViewDelegate, UITableViewDataSource> dataSource;
-(void)userDidSelectTopicNotification:(NSNotification *)notification;

@property (strong) MyRedditObjectConfiguration *objectConfiguration;
@property (strong) AppManager *manager;

@end
