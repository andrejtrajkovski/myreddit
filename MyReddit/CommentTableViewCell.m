//
//  CommentTableViewCell.m
//  MyReddit
//
//  Created by Andrej Trajkovski on 18.12.17.
//  Copyright © 2017 Andrej Trajkovski. All rights reserved.
//

#import "CommentTableViewCell.h"

@implementation CommentTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
