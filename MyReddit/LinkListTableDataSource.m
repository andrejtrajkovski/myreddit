//
//  PostListTableDataSource.m
//  MyReddit
//
//  Created by Andrej Trajkovski on 24.11.17.
//  Copyright © 2017 Andrej Trajkovski. All rights reserved.
//

#import "LinkListTableDataSource.h"
#import "Topic.h"
#import "LinkTableViewCell.h"
#import "Link.h"
#import <UIImageView+AFNetworking.h>

@interface LinkListTableDataSource()<LinkTableViewCellDelegate>

@end

@implementation LinkListTableDataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (self.topic.links) {
        return self.topic.links.count == 0 ? 1 : self.topic.links.count;
    }
    return 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (self.topic.links.count == 0){
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: placeholderCellReuseIdentifier forIndexPath:indexPath];
        cell.textLabel.text = @"There was a problem.";

        return cell;
        
    }else{
        
        Link *link = [self linkForIndexPath:indexPath];
        LinkTableViewCell *linkCell = [tableView dequeueReusableCellWithIdentifier:[LinkTableViewCell cellReuseIdentifier] forIndexPath:indexPath];
        
        linkCell.delegate = self;
        linkCell.indexPath = indexPath;
        linkCell.titleLabel.text = link.title;
        linkCell.scoreLabel.text = [NSString stringWithFormat:@"%@", link.score];
        [linkCell.thumbnailImageView setImageWithURL:link.thumbnail];
        
        return linkCell;
    }
}

-(Link *)linkForIndexPath:(NSIndexPath *)indexPath{
    
    return [self.topic.links objectAtIndex:indexPath.row];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    Link *selectedLink = [self linkForIndexPath:indexPath];
    NSNotification *selectedLinkNotification = [NSNotification notificationWithName:LinkTableDidSelectLinkNotification object:selectedLink];
    [[NSNotificationCenter defaultCenter] postNotification:selectedLinkNotification];
}

-(void)didSelectCommentsForCellAtIndexPath:(NSIndexPath *)indexPath{
    
    Link *selectedLink = [self linkForIndexPath:indexPath];
    NSNotification *selectedLinkNotification = [NSNotification notificationWithName:LinkTableDidSelectCommentsNotification object:selectedLink];
    [[NSNotificationCenter defaultCenter] postNotification:selectedLinkNotification];
}

@end

NSString *placeholderCellReuseIdentifier = @"placeholder";
NSString *LinkTableDidSelectLinkNotification = @"LinkTableDidSelectLinkNotification";
NSString *LinkTableDidSelectCommentsNotification = @"LinkTableDidSelectCommentsNotification";
