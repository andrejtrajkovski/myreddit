//
//  CommentTableViewCell.h
//  MyReddit
//
//  Created by Andrej Trajkovski on 18.12.17.
//  Copyright © 2017 Andrej Trajkovski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommentTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *commentLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leadingMargin;

@end
