//
//  MyRedditViewController.m
//  MyReddit
//
//  Created by Andrej Trajkovski on 27.9.17.
//  Copyright © 2017 Andrej Trajkovski. All rights reserved.
//

#import "MyRedditViewController.h"
#import "TopicTableDataSource.h"
#import "LinkListTableDataSource.h"
#import "LinkTableViewCell.h"
#import "UITableViewCell+Utility.h"
#import "AppManager.h"
#import "MyRedditObjectConfiguration.h"
#import "CommentListTableDataSource.h"
#import "CommentTableViewCell.h"
#import "WebViewViewController.h"

@interface MyRedditViewController ()

@end

@implementation MyRedditViewController

@synthesize tableView, dataSource, objectConfiguration;

- (void)viewDidLoad {
    
    [super viewDidLoad];
    self.tableView.dataSource = dataSource;
    self.tableView.delegate = dataSource;
    
    if ([self.dataSource isKindOfClass:[LinkListTableDataSource class]]) {
        [self.tableView registerNib:[LinkTableViewCell nib] forCellReuseIdentifier:[LinkTableViewCell cellReuseIdentifier]];
        [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:placeholderCellReuseIdentifier];
    }else if ([self.dataSource isKindOfClass:[CommentListTableDataSource class]]) {
        [self.tableView registerNib:[CommentTableViewCell nib] forCellReuseIdentifier:[CommentTableViewCell cellReuseIdentifier]];
    }
    
    [self.tableView setSeparatorColor:[UIColor lightGrayColor]];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    self.manager = [objectConfiguration appManager];
    self.manager.delegate = self;
    if ([self.dataSource isKindOfClass:[LinkListTableDataSource class]]) {
        Topic *selectedTopic =  [(LinkListTableDataSource *)self.dataSource topic];
        [self.manager fetchLinksOnTopic:selectedTopic];
        self.title = selectedTopic.name;
    }else if ([self.dataSource isKindOfClass:[CommentListTableDataSource class]]) {
        Link *selectedLink = [(CommentListTableDataSource *)self.dataSource link];
        [self.manager fetchCommentsForLink:selectedLink];
        self.title = selectedLink.title;
    }
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    [[NSNotificationCenter defaultCenter]
     addObserver: self
     selector: @selector(userDidSelectTopicNotification:) name: TopicTableDidSelectTopicNotification
     object: nil];
    [[NSNotificationCenter defaultCenter]
     addObserver: self
     selector: @selector(userDidSelectLinkNotification:) name: LinkTableDidSelectLinkNotification
     object: nil];
    [[NSNotificationCenter defaultCenter]
     addObserver: self
     selector: @selector(userDidSelectCommentsLinkNotification:) name: LinkTableDidSelectCommentsNotification
     object: nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver: self name: TopicTableDidSelectTopicNotification object: nil];
    [[NSNotificationCenter defaultCenter] removeObserver: self name: LinkTableDidSelectLinkNotification object: nil];
    [[NSNotificationCenter defaultCenter] removeObserver: self name: LinkTableDidSelectCommentsNotification object: nil];
}

-(void)userDidSelectTopicNotification:(NSNotification *)notification{
    
    Topic * selectedTopic = (Topic *)[notification object];
    MyRedditViewController *nextViewController = [[MyRedditViewController alloc] init];
    nextViewController.objectConfiguration = self.objectConfiguration;
    LinkListTableDataSource *linksDataSource = [[LinkListTableDataSource alloc] init];
    linksDataSource.topic = selectedTopic;
    nextViewController.dataSource = linksDataSource;
    [[self navigationController] pushViewController: nextViewController animated: NO];
}

-(void)userDidSelectCommentsLinkNotification:(NSNotification *)notification{
    
    Link * selectedLink = (Link *)[notification object];
    MyRedditViewController *nextViewController = [[MyRedditViewController alloc] init];
    nextViewController.objectConfiguration = self.objectConfiguration;
    CommentListTableDataSource *linksDataSource = [[CommentListTableDataSource alloc] init];
    linksDataSource.link = selectedLink;
    nextViewController.dataSource = linksDataSource;
    [[self navigationController] pushViewController: nextViewController animated: NO];
}

-(void)userDidSelectLinkNotification:(NSNotification *)notification{
    
    Link * selectedLink = (Link *)[notification object];
    WebViewViewController *wvvc = [WebViewViewController new];
    wvvc.link = selectedLink;
    [self.navigationController pushViewController:wvvc animated:NO];
}

#pragma mark - AppManagerDelegate implementation

-(void)fetchingLinksFailedWithError:(NSError *)error{
    
}

-(void)didReceiveLinks:(NSArray *)links{
    [links enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [[(LinkListTableDataSource *)self.dataSource topic] addLink:obj];
    }];
    [tableView reloadData];
}

-(void)fetchingCommentsFailedWithError:(NSError *)error{
    
}

-(void)didAddComentsForLink:(Link *)link{
    [tableView reloadData];
}

@end
