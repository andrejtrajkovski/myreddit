//
//  CommentBuilder.h
//  MyReddit
//
//  Created by Andrej Trajkovski on 29.8.17.
//  Copyright © 2017 Andrej Trajkovski. All rights reserved.
//

#import <Foundation/Foundation.h>

enum {
    CommentBuilderInvalidJSONError = 0,
    CommentBuilderMissingDataError,
    CommentBuilderWrongLinkError
};

@class Link;
@interface CommentBuilder : NSObject

- (void)addCommentsToLink: (Link *)link fromJSON: (NSString *)objectNotation error: (NSError **)error;
//-(NSArray *)commentsFromJSON: (NSString *)objectNotation
//                       error: (NSError **)error;

@end
