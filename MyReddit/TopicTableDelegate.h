//
//  TopicTableDelegate.h
//  MyReddit
//
//  Created by Andrej Trajkovski on 09.11.17.
//  Copyright © 2017 Andrej Trajkovski. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TopicTableDataSource;

@interface TopicTableDelegate : NSObject <UITableViewDelegate>

@end
