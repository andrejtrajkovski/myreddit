//
//  LinkTableViewCell.h
//  MyReddit
//
//  Created by Andrej Trajkovski on 24.11.17.
//  Copyright © 2017 Andrej Trajkovski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITableViewCell+Utility.h"

@protocol LinkTableViewCellDelegate

-(void)didSelectCommentsForCellAtIndexPath:(NSIndexPath *)indexPath;

@end

@interface LinkTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *thumbnailImageView;
@property (weak, nonatomic) IBOutlet UIButton *commentsButton;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (weak, nonatomic) id <LinkTableViewCellDelegate> delegate;
@property (strong, nonatomic) NSIndexPath *indexPath;

@end
