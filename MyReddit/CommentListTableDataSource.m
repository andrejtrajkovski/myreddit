//
//  CommentListTableDataSource.m
//  MyReddit
//
//  Created by Andrej Trajkovski on 18.12.17.
//  Copyright © 2017 Andrej Trajkovski. All rights reserved.
//

#import "CommentListTableDataSource.h"
#import "Link.h"
#import "CommentTableViewCell.h"
#import "Comment.h"
#import "UITableViewCell+Utility.h"

@interface CommentListTableDataSource()

@property (strong, nonatomic) NSArray *allCommentsArray;

@end

@implementation CommentListTableDataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
//    NSInteger sum = [self sumOfCommentsForPost:self.link];
//    return sum;
    return self.allCommentsArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CommentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[CommentTableViewCell cellReuseIdentifier] forIndexPath:indexPath];
    Comment *comment = [self.allCommentsArray objectAtIndex:indexPath.row];
    cell.commentLabel.text = comment.title;
    cell.leadingMargin.constant = (comment.depth + 1) * 8;
    [cell setNeedsUpdateConstraints];
    
    return cell;
}

-(NSInteger)sumOfCommentsForPost:(Post *)post{
    
    __block NSInteger sum = 0;
    sum = sum + post.comments.count;
    [post.comments enumerateObjectsUsingBlock:^(Comment * obj, NSUInteger idx, BOOL * _Nonnull stop) {
        sum = sum + [self sumOfCommentsForPost:obj];
    }];
    
    return sum;
}

-(NSArray *)allCommentsArray{
    
    if (!_allCommentsArray && self.link.comments) {
        NSMutableArray *helperArray = [NSMutableArray new];
        helperArray = [self arrayByAddingCommentsFromPost:self.link
                                                  toArray:helperArray];
        _allCommentsArray = [NSArray arrayWithArray:helperArray];
    }
    
    return _allCommentsArray;
}

-(NSMutableArray *)arrayByAddingCommentsFromPost:(Post *)post toArray:(NSMutableArray *)arrayOfComments{
    
    __block NSMutableArray *helperBlockArray = [NSMutableArray arrayWithArray:arrayOfComments];
//    [helperBlockArray addObjectsFromArray:post.comments];
    [post.comments enumerateObjectsUsingBlock:^(Comment *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [helperBlockArray addObject:obj];
        helperBlockArray = [self arrayByAddingCommentsFromPost:obj
                                                  toArray:helperBlockArray];
        
    }];
    
    return helperBlockArray;
}

@end

