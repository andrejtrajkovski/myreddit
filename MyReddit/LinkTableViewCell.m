//
//  LinkTableViewCell.m
//  MyReddit
//
//  Created by Andrej Trajkovski on 24.11.17.
//  Copyright © 2017 Andrej Trajkovski. All rights reserved.
//

#import "LinkTableViewCell.h"

@implementation LinkTableViewCell

- (IBAction)commentsTouch:(UIButton *)sender {
    
    [self.delegate didSelectCommentsForCellAtIndexPath:self.indexPath];
}

@end
