//
//  RedditCommunicator.m
//  MyReddit
//
//  Created by Andrej Trajkovski on 09.12.15.
//  Copyright © 2015 Andrej Trajkovski. All rights reserved.
//

#import "RedditCommunicator.h"

@interface RedditCommunicator()

@end

@implementation RedditCommunicator

@synthesize delegate, receivedData;

-(void)searchForLinksOnTopic:(Topic *)topic{
    
    NSString *fetchUrlString = [NSString stringWithFormat:@"https://www.reddit.com/r/%@.json", [topic name]];
    [self fetchContentAtUrl:[NSURL URLWithString:fetchUrlString]
     errorHandler:^(NSError * error) {
         
         [delegate searchingForLinksFailedWithError:error];
     } successHandler:^(NSString *objectNotation) {
         
         [delegate receivedLinksJSON:objectNotation];
     }];
}

-(void)searchForCommentsOnLink:(Link *)link{
    
    NSString *fetchUrlString = [NSString stringWithFormat:@"https://www.reddit.com/comments/%@.json",[link identifier]];
    [self fetchContentAtUrl:[NSURL URLWithString:fetchUrlString]
       errorHandler:^(NSError * error) {
           
           [delegate searchingForCommentsFailedWithError:error];
       } successHandler:^(NSString *objectNotation) {
           
           [delegate receivedCommentsJSON:objectNotation];
       }];
}

-(void)fetchContentAtUrl:(NSURL *)url errorHandler:(void (^)(NSError *))errorBlock successHandler:(void (^)(NSString *))successBlock {
    
    fetchURL = url;
    
    errorHandler = [errorBlock copy];
    successHandler = [successBlock copy];
    
    NSURLRequest *request = [NSURLRequest requestWithURL: fetchURL];
    
    [self launchConnectionForRequest: request];
}
    
- (void)launchConnectionForRequest: (NSURLRequest *)request  {
    
    [self cancelAndDiscardURLConnection];
    fetchingConnection = [NSURLConnection connectionWithRequest: request delegate: self];
    
    NSLog(@"fc absoluteString %@", [[[fetchingConnection currentRequest] URL] absoluteString]);
}

- (void)cancelAndDiscardURLConnection {
    
    [fetchingConnection cancel];
    fetchingConnection = nil;
}

#pragma mark NSURLConnection Delegate

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    receivedData = nil;
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
    if ([httpResponse statusCode] != 200) {
        NSError *error = [NSError errorWithDomain: RedditCommunicatorErrorDomain code: [httpResponse statusCode] userInfo: nil];
        errorHandler(error);
        [self cancelAndDiscardURLConnection];
    }
    else {
        receivedData = [[NSMutableData alloc] init];
    }
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    receivedData = nil;
    fetchingConnection = nil;
    fetchURL = nil;
    errorHandler(error);
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    fetchingConnection = nil;
    fetchURL = nil;
    NSString *receivedText = [[NSString alloc] initWithData: receivedData
                                                   encoding: NSUTF8StringEncoding];
    receivedData = nil;
    successHandler(receivedText);
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [receivedData appendData: data];
}

@end

NSString *RedditCommunicatorErrorDomain = @"RedditCommunicatorErrorDomain";

