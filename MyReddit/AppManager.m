//
//  AppManager.m
//  MyReddit
//
//  Created by Andrej Trajkovski on 01.12.15.
//  Copyright © 2015 Andrej Trajkovski. All rights reserved.
//

#import "AppManager.h"

@implementation AppManager
@synthesize delegate;

-(void)setDelegate:(id<AppManagerDelegate>)newDelegate{
    
    if (newDelegate && ![newDelegate conformsToProtocol:
          @protocol(AppManagerDelegate)]) {
        [[NSException exceptionWithName: NSInvalidArgumentException
                                 reason:
          @"Delegate object does not conform to the delegate protocol"
                               userInfo: nil] raise];
    }
    delegate = newDelegate;
}

#pragma mark - Links

-(void)fetchLinksOnTopic:(Topic *)topic{
    [self.communicator searchForLinksOnTopic:topic];
}

- (void)searchingForLinksFailedWithError: (NSError *)error{
    [self tellDelegateAboutLinkSearchError: error];
}

-(void)tellDelegateAboutLinkSearchError:(NSError *)underlyingError{
    NSDictionary *errorInfo = nil;
    if (underlyingError) {
        errorInfo = [NSDictionary dictionaryWithObject: underlyingError forKey: NSUnderlyingErrorKey];
    }
    NSError *reportableError = [NSError errorWithDomain: AppManagerError code: AppManagerErrorLinkSearchCode userInfo: errorInfo];
    [delegate fetchingLinksFailedWithError:reportableError];
}

-(void)receivedLinksJSON:(NSString *)JSON{
    NSError *linkBuilderError = nil;
    NSArray *links = [self.linkBuilder linksFromJSON:JSON error:&linkBuilderError];
    if (!links) {
        [self tellDelegateAboutLinkSearchError:linkBuilderError];
    }else{
        [delegate didReceiveLinks:links];
    }
}

#pragma mark - Comments

-(void)fetchCommentsForLink:(Link *)link{
    
    self.linkNeedingComments = link;
    [self.communicator searchForCommentsOnLink:link];
}

-(void)receivedCommentsJSON:(NSString *)JSON{
    
    NSError *commentBuilderError = nil;
    [self.commentBuilder addCommentsToLink:self.linkNeedingComments fromJSON:JSON error:&commentBuilderError];
    if (commentBuilderError) {
        [self searchingForCommentsFailedWithError:commentBuilderError];
    }else{
        [delegate didAddComentsForLink:self.linkNeedingComments];
    }
}

- (void)searchingForCommentsFailedWithError: (NSError *)error{
    
    self.linkNeedingComments = nil;
    NSDictionary *userInfo = nil;
    if (error) {
        userInfo = [NSDictionary dictionaryWithObject: error forKey: NSUnderlyingErrorKey];
    }
    NSError *reportableError = [NSError errorWithDomain: AppManagerError code:AppManagerErrorLinkCommentsFetchCode userInfo: userInfo];
    [delegate fetchingCommentsFailedWithError: reportableError];

}

NSString *AppManagerError = @"AppManagerError";

@end
