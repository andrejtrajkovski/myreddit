//
//  AppManagerDelegate.h
//  MyReddit
//
//  Created by Andrej Trajkovski on 01.12.15.
//  Copyright © 2015 Andrej Trajkovski. All rights reserved.
//

#import <Foundation/Foundation.h>
@class Link;

@protocol AppManagerDelegate <NSObject>

-(void)fetchingLinksFailedWithError:(NSError *)error;
-(void)didReceiveLinks:(NSArray *)questions;

-(void)fetchingCommentsFailedWithError:(NSError *)error;
-(void)didAddComentsForLink:(Link *)link;

@end
