//
//  CommentListTableDataSource.h
//  MyReddit
//
//  Created by Andrej Trajkovski on 18.12.17.
//  Copyright © 2017 Andrej Trajkovski. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Link;

@interface CommentListTableDataSource : NSObject <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) Link *link;

@end
