//
//  Comment.m
//  MyReddit
//
//  Created by Andrej Trajkovski on 11.10.15.
//  Copyright © 2015 г. Andrej Trajkovski. All rights reserved.
//

#import "Comment.h"

@implementation Comment

-(instancetype)init{
    
    self = [super init];
    
    if(self){
        self.postType = PostTypeComment;
    }
    
    return self;
}

@end
