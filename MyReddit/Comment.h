//
//  Comment.h
//  MyReddit
//
//  Created by Andrej Trajkovski on 11.10.15.
//  Copyright © 2015 г. Andrej Trajkovski. All rights reserved.
//

#import "Post.h"

@interface Comment : Post

@property (strong, nonatomic) NSString *fatherId;
@property (assign, nonatomic) NSInteger depth;

@end
