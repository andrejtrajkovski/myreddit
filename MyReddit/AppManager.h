//
//  AppManager.h
//  MyReddit
//
//  Created by Andrej Trajkovski on 01.12.15.
//  Copyright © 2015 Andrej Trajkovski. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppManagerDelegate.h"
#import "RedditCommunicator.h"
#import "Topic.h"
#import "LinkBuilder.h"
#import "CommentBuilder.h"

enum {
    AppManagerErrorLinkSearchCode,
    AppManagerErrorLinkCommentsFetchCode,
    AppManagerErrorAnswerFetchCode
};

@interface AppManager : NSObject <RedditCommunicatorDelegate>

@property (strong, nonatomic) CommentBuilder *commentBuilder;
@property (strong, nonatomic) LinkBuilder *linkBuilder;
@property (strong, nonatomic) RedditCommunicator *communicator;
@property (weak, nonatomic) id<AppManagerDelegate> delegate;

@property (strong, nonatomic) Link *linkNeedingComments;

-(void)fetchLinksOnTopic:(Topic *)topic;
-(void)searchingForLinksFailedWithError:(NSError *)error;
-(void)receivedLinksJSON:(NSString *)JSON;
-(void)tellDelegateAboutLinkSearchError:(NSError *)underlyingError;

-(void)fetchCommentsForLink:(Link *)link;
-(void)receivedCommentsJSON:(NSString *)JSON;
-(void)searchingForCommentsFailedWithError:(NSError *)error;

@end
