//
//  MyRedditObjectConfiguration.m
//  MyReddit
//
//  Created by Andrej Trajkovski on 28.11.17.
//  Copyright © 2017 Andrej Trajkovski. All rights reserved.
//

#import "MyRedditObjectConfiguration.h"
#import "AppManager.h"
#import "RedditCommunicator.h"
#import "LinkBuilder.h"
#import "CommentBuilder.h"

@implementation MyRedditObjectConfiguration

-(AppManager *)appManager{
    AppManager *manager = [[AppManager alloc] init];
    manager.communicator = [[RedditCommunicator alloc] init];
    manager.communicator.delegate = manager;
    manager.linkBuilder = [[LinkBuilder alloc] init];
    manager.commentBuilder = [[CommentBuilder alloc] init];
    return manager;
}

@end
