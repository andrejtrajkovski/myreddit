# Description #

A simple iOS app, developed using a test driven approach and the Facade design pattern.  It uses the Reddit.com API to list posts and comments about 5 subreddits.  The purpose of this project was mastering test-driven development and get a better sense of unit testing in an iOS project.  It uses OCMock and XCTest as testing frameworks.

## Model Classes UML: ##

![alt text](Diagrams/models.png "Model classes UML")

## Facade: ##

![alt text](Diagrams/Facade.png "Facade")

## Controller Logic: ##

![alt text](Diagrams/controllers.png "Controller logic")
