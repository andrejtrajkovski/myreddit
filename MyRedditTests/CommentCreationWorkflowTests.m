//
//  CommentCreationWorkflowTests.m
//  MyReddit
//
//  Created by Andrej Trajkovski on 08.9.17.
//  Copyright © 2017 Andrej Trajkovski. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>
#import "AppManager.h"
#import "RedditCommunicator.h"
#import "Comment.h"
#import "MockAppManagerDelegate.h"

@interface CommentCreationWorkflowTests : XCTestCase

@end

@implementation CommentCreationWorkflowTests{
    
    NSError *underlyingError;
    AppManager *mgr;
    id ocMockAppManagerDelegate;
    id commentBuilderMock;
    NSArray *commentsArray;
}

- (void)setUp {
    [super setUp];
    
    commentBuilderMock = [OCMockObject mockForClass:[CommentBuilder class]];
    
    mgr = [AppManager new];
    
    ocMockAppManagerDelegate = [OCMockObject mockForProtocol:@protocol(AppManagerDelegate)];
//    ocMockAppManagerDelegate = OCMStrictProtocolMock(@protocol(AppManagerDelegate));

    mgr.delegate = ocMockAppManagerDelegate;
    
    mgr.commentBuilder = commentBuilderMock;
    
    underlyingError = [NSError errorWithDomain: @"Test domain"
                                          code: 0 userInfo: nil];
    
    id comment = [OCMockObject mockForClass:[Comment class]];
    commentsArray = [NSArray arrayWithObject:comment];

}

- (void)tearDown {
    
    commentBuilderMock = nil;
    mgr = nil;
    ocMockAppManagerDelegate = nil;
    underlyingError = nil;
    commentsArray = nil;
    [super tearDown];
}

-(void)testFetchingCommentsMeansRequestingJSON{

    id mockCommunicator = OCMClassMock([RedditCommunicator class]);
    mgr.communicator = mockCommunicator;
    [mgr fetchCommentsForLink:[OCMArg any]];
    OCMVerify([mockCommunicator searchForCommentsOnLink:[OCMArg any]]);
}

-(void)testFailSearchingForCommentsNotifiesManager{
    
    [[ocMockAppManagerDelegate expect] fetchingCommentsFailedWithError:[OCMArg any]];
    [mgr searchingForCommentsFailedWithError:underlyingError];
    OCMVerifyAll(ocMockAppManagerDelegate);
}

-(void)testErrorReturnedToDelegateIsNotErrorNotifiedByCommunicator{
    
    [[ocMockAppManagerDelegate expect] fetchingCommentsFailedWithError:[OCMArg checkWithBlock:^BOOL(id value){
        return ![underlyingError isEqual:value];
    }]];
    [mgr searchingForCommentsFailedWithError:underlyingError];
    OCMVerifyAll(ocMockAppManagerDelegate);
}

- (void)testErrorReturnedToDelegateDocumentsUnderlyingError {
    [[ocMockAppManagerDelegate expect] fetchingLinksFailedWithError:[OCMArg checkWithBlock:^BOOL(NSError *reportableError){
        NSLog(@"rep error underlying %@",[[[reportableError userInfo] objectForKey:NSUnderlyingErrorKey] domain]);
        return [underlyingError isEqual:[[reportableError userInfo] objectForKey:NSUnderlyingErrorKey]];
    }]];
    [mgr searchingForLinksFailedWithError: underlyingError];
    OCMVerifyAll(ocMockAppManagerDelegate);
}

- (void)testCommentJSONIsPassedToCommentBuilder {
    
    id niceOcMockAppManagerDelegate = [OCMockObject niceMockForProtocol:@protocol(AppManagerDelegate)];
    mgr.delegate = niceOcMockAppManagerDelegate;
    NSString *fakeJSONString = @"Fake JSON";
    NSError *dummyError = [NSError errorWithDomain: @"Dummy error"
                                              code: 0 userInfo: nil];
    [[commentBuilderMock expect] addCommentsToLink:[OCMArg any] fromJSON:[OCMArg checkWithBlock:^BOOL(NSString *jsonSentToBuilder){
        
        return [jsonSentToBuilder isEqual:fakeJSONString];
        
    }] error:[OCMArg setTo:dummyError]];
    
    [mgr receivedCommentsJSON:fakeJSONString];

    OCMVerifyAll(commentBuilderMock);
}

-(void)testDelegateNotifiedOfErrorWhenCommentBuilderFails {
    
    NSError *dummyError = [NSError errorWithDomain: @"AppManagerError" code: 0 userInfo: nil];
    
    [[commentBuilderMock expect] addCommentsToLink:[OCMArg any] fromJSON:@"Fake JSON" error:[OCMArg setTo:dummyError]];
    
    [[ocMockAppManagerDelegate expect] fetchingCommentsFailedWithError:[OCMArg checkWithBlock:^BOOL(NSError *error) {
        
        return [[error userInfo] objectForKey:NSUnderlyingErrorKey] == dummyError;
        
    }]];
    
    [mgr receivedCommentsJSON:@"Fake JSON"];
    
    OCMVerifyAll(ocMockAppManagerDelegate);
}

- (void)testDelegateNotToldAboutErrorWhenCommentsReceived {
    
    id niceOcMockAppManagerDelegate = [OCMockObject niceMockForProtocol:@protocol(AppManagerDelegate)];
    [[niceOcMockAppManagerDelegate reject] fetchingCommentsFailedWithError:[OCMArg any]];
    
    mgr.delegate = niceOcMockAppManagerDelegate;
    
    OCMStub([commentBuilderMock addCommentsToLink:[OCMArg any] fromJSON:[OCMArg any] error:[OCMArg setTo:nil]]);
    [mgr receivedCommentsJSON:[OCMArg any]];
    
    OCMVerifyAll(niceOcMockAppManagerDelegate);
}

- (void)testDelegateAddsTheCommentsDiscoveredByManagerToLink {
    
    id mockAppManagerDelegate = [OCMockObject mockForProtocol:@protocol(AppManagerDelegate)];
    mgr.delegate = mockAppManagerDelegate;

    [[mockAppManagerDelegate expect] didAddComentsForLink:[OCMArg any]];
    
    OCMStub([commentBuilderMock addCommentsToLink:[OCMArg any] fromJSON:[OCMArg any] error:[OCMArg setTo:nil]]);
    [mgr receivedCommentsJSON:[OCMArg any]];
    
    OCMVerifyAll(mockAppManagerDelegate);
}

- (void)testManagerSavesLinkThatNeedsComments{
    
    Link *link = [Link new];
    
    [mgr fetchCommentsForLink:link];
    
    XCTAssertEqual(link, mgr.linkNeedingComments);
}

-(void)testCommunicatorAsksForCommentsOnCorrectLink{
    
    Link *link = [Link new];
    
    id mockCommunicator = [OCMockObject mockForClass:[RedditCommunicator class]];
    mgr.communicator = mockCommunicator;

    [[mockCommunicator expect]searchForCommentsOnLink:[OCMArg checkWithBlock:^BOOL(id obj) {
        return [obj isEqual:link];
    }]];
    
    [mgr fetchCommentsForLink:link];
    
    OCMVerifyAll(mockCommunicator);
}

-(void)testCommentBuilderGetsPassedSameLink{
    
    Link *link = [Link new];
    
    [[commentBuilderMock expect]addCommentsToLink:[OCMArg checkWithBlock:^BOOL(id obj) {
        return [obj isEqual:link];
    }] fromJSON:[OCMArg any]
                                            error:[OCMArg setTo:nil]];
    
    [[ocMockAppManagerDelegate stub] didAddComentsForLink:[OCMArg any]];
    
    [mgr fetchCommentsForLink:link];
    [mgr receivedCommentsJSON:@""];
    
    OCMVerifyAll(commentBuilderMock);
}

-(void)testDelegateReturnsSameLink{
    
    Link *link = [Link new];
    
    [[ocMockAppManagerDelegate expect] didAddComentsForLink:[OCMArg checkWithBlock:^BOOL(id obj) {
        return [obj isEqual:link];
    }]];
    
    [[commentBuilderMock stub] addCommentsToLink:[OCMArg any] fromJSON:[OCMArg any] error:[OCMArg setTo:nil]];
    
    [mgr fetchCommentsForLink:link];
    [mgr receivedCommentsJSON:@""];
    
    OCMVerifyAll(ocMockAppManagerDelegate);
}

-(void)testCommentFailRemovesLinkNeedingComments{
    
    ocMockAppManagerDelegate = [OCMockObject niceMockForProtocol:@protocol(AppManagerDelegate)];
    mgr.delegate = ocMockAppManagerDelegate;
    
    Link *link = [Link new];
    NSError *dummyError = [NSError errorWithDomain: @"Dummy error"
                                              code: 0 userInfo: nil];

    [mgr fetchCommentsForLink:link];
    [mgr searchingForCommentsFailedWithError:dummyError];
    
    XCTAssertNil(mgr.linkNeedingComments);
}

@end
