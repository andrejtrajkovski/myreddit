//
//  TopicTableDelegateTests.m
//  MyRedditTests
//
//  Created by Andrej Trajkovski on 20.11.17.
//  Copyright © 2017 Andrej Trajkovski. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "TopicTableDelegate.h"
#import "TopicTableDataSource.h"
#import "Topic.h"
#import <OCMock/OCMock.h>

@interface TopicTableDelegateTests : XCTestCase

@end

@implementation TopicTableDelegateTests
{
    NSNotification *receivedNotification;
    TopicTableDataSource *dataSource;
    Topic *redDevilsTopic;
}

- (void)setUp {
    [super setUp];
    
    dataSource = [TopicTableDataSource new];
    redDevilsTopic = [[Topic alloc] initWithName:@"reddevils"];
    [dataSource setTopics:@[redDevilsTopic]];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(didReceiveNotification:)
     name:TopicTableDidSelectTopicNotification
     object:nil];
}

- (void)tearDown {
    
    dataSource = nil;
    redDevilsTopic = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [super tearDown];
}

-(void)didReceiveNotification:(NSNotification *)notification{
    
    receivedNotification = notification;
}

- (void)testDelegatePostsNotificationOnSelectionShowingWhichTopicWasSelected {
    
    NSIndexPath *selection = [NSIndexPath indexPathForRow:0
                                                inSection:0];
    [dataSource tableView:[OCMArg any] didSelectRowAtIndexPath:selection];
    XCTAssertEqualObjects([receivedNotification name], TopicTableDidSelectTopicNotification);
    XCTAssertEqualObjects([receivedNotification object], redDevilsTopic);
}

@end
