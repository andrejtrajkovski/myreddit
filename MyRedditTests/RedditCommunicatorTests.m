//
//  RedditCommunicatorTests.m
//  MyReddit
//
//  Created by Andrej Trajkovski on 22.8.17.
//  Copyright © 2017 Andrej Trajkovski. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "InspectableRedditCommunicator.h"
#import <OCMock/OCMock.h>
#import "AFNetworking.h"
#import "Topic.h"
#import "Link.h"
#import "AppManager.h"

@interface RedditCommunicatorTests : XCTestCase

@end

@implementation RedditCommunicatorTests{

    InspectableRedditCommunicator *communicator;
    id redDevilsTopic;
    NSString *topicName;
    id manager;

    id dummyUrlConnection;
    id partialMock;
}

- (void)setUp {
    [super setUp];
    
    topicName = @"reddevils";
    redDevilsTopic = OCMClassMock([Topic class]);
    OCMStub([redDevilsTopic name]).andReturn(topicName);

    communicator = [InspectableRedditCommunicator new];
    manager = OCMClassMock([AppManager class]);
    
    communicator.delegate = manager;
    
    dummyUrlConnection = OCMClassMock([NSURLConnection class]);
    partialMock = [OCMockObject partialMockForObject:communicator];
    [[[partialMock stub] andReturn:dummyUrlConnection] fetchingConnection];
    
}

- (void)tearDown {
    
    manager = nil;
    topicName = nil;
    redDevilsTopic = nil;
    communicator = nil;
    [partialMock stopMocking];
    [super tearDown];
}

-(void)testSearchingForLinksOnTopicCallsTopicAPI{
    
    NSString *fetchUrlString = [NSString stringWithFormat:@"https://www.reddit.com/r/%@.json", topicName];
    
    [communicator searchForLinksOnTopic:redDevilsTopic];
    
    XCTAssertEqualObjects(fetchUrlString, [[communicator fetchURL] absoluteString], @"Wrong topic URL. See the topic API.");
}

-(void)testSearchingForLinkCommentsCallsCommentsAPI{
    
    NSString *linkId = @"6wj2lk";
    NSString *fetchUrlString = [NSString stringWithFormat:@"https://www.reddit.com/comments/%@.json", linkId];
    
    id valenciaLink = OCMClassMock([Link class]);
    OCMStub([valenciaLink identifier]).andReturn(linkId);
    
    [communicator searchForCommentsOnLink:valenciaLink];
    
    XCTAssertEqualObjects(fetchUrlString, [[communicator fetchURL] absoluteString], @"Wrong comments URL. See the comments API.");
}

- (void)testSearchingForLinksCreatesURLConnection {
    
    [communicator searchForLinksOnTopic:redDevilsTopic];
    XCTAssertNotNil([communicator fetchingConnection], @"There should be a URL connection in-flight now.");
}

- (void)testStartingNewSearchThrowsOutOldConnection {
    
    [partialMock stopMocking];
    NSString *soccerName = @"soccer";
    
    id soccerTopic = OCMClassMock([Topic class]);
    OCMStub([soccerTopic name]).andReturn(soccerName);

    [communicator searchForLinksOnTopic: redDevilsTopic];
    NSURLConnection *firstConnection = [communicator fetchingConnection];
    [communicator searchForLinksOnTopic: soccerTopic];
    XCTAssertFalse([[communicator fetchingConnection] isEqual: firstConnection], @"The communicator needs to replace its URL connection to start a new one");
}

#pragma mark - partial

- (void)testReceivingResponseDiscardsExistingData {
    
    [communicator setReceivedData:[[@"Hellooooo"
                                    dataUsingEncoding: NSUTF8StringEncoding] mutableCopy]];
    
    [communicator searchForLinksOnTopic:redDevilsTopic];
    [communicator connection: nil didReceiveResponse: nil];
    
    XCTAssertEqual([communicator.receivedData length], (NSUInteger)0,
               @"Data should have been discarded");
}

- (void)testReceivingResponseWith404StatusPassesErrorToDelegate {
    
    id fourOhFourResponse = OCMClassMock([NSHTTPURLResponse class]);
    [[[fourOhFourResponse stub]andReturnValue:OCMOCK_VALUE(404)] statusCode];
    [[manager expect]searchingForLinksFailedWithError:[OCMArg checkWithBlock:^BOOL(NSError* obj) {
        
        return obj.code == 404;
    }]];
    
    [communicator searchForLinksOnTopic:redDevilsTopic];
    [communicator connection: nil didReceiveResponse:fourOhFourResponse];
    
    [manager verify];
}

- (void)testNoErrorReceivedOn200Status {

    id twoHundredResponse = OCMClassMock([NSHTTPURLResponse class]);
    [[[twoHundredResponse stub]andReturnValue:OCMOCK_VALUE(200)] statusCode];
    [[manager reject] searchingForLinksFailedWithError:[OCMArg any]];
    
    [communicator searchForLinksOnTopic:redDevilsTopic];
    [communicator connection: nil didReceiveResponse:twoHundredResponse];
    
    [manager verify];
}

- (void)testConnectionFailingPassesErrorToDelegate {
    
    id errorMock = OCMClassMock([NSError class]);
    [[[errorMock stub] andReturnValue:OCMOCK_VALUE(12345)] code];
    [[[errorMock stub] andReturn:OCMOCK_ANY] domain];
    [[manager expect]searchingForLinksFailedWithError:[OCMArg checkWithBlock:^BOOL(NSError* obj) {
        
        return obj.code == 12345;
    }]];
    
    [communicator searchForLinksOnTopic:redDevilsTopic];
    [communicator connection: nil didFailWithError: errorMock];
    
    [manager verify];
}

- (void)testSuccessfulLinkSearchPassesDataToDelegate {
    
    NSString *string = @"some dummy text";
    NSMutableData *rcData = [[string dataUsingEncoding:NSUTF8StringEncoding] mutableCopy];
    
    [communicator setReceivedData:rcData];
    [[manager expect]receivedLinksJSON:[OCMArg checkWithBlock:^BOOL(id obj) {
        
        return [obj isEqualToString:string];
    }]];
    
    [communicator searchForLinksOnTopic:redDevilsTopic];
    [communicator connectionDidFinishLoading: nil];
    
    [manager verify];
}

- (void)testAdditionalDataAppendedToDownload {
    
    NSString *string = @"some dummy text";
    NSMutableData *rcData = [[string dataUsingEncoding:NSUTF8StringEncoding] mutableCopy];
    [communicator setReceivedData: rcData];
    NSData *extraData = [@" appended" dataUsingEncoding: NSUTF8StringEncoding];
    
    [communicator connection: nil didReceiveData: extraData];
    NSString *combinedString = [[NSString alloc]
                                initWithData: [communicator receivedData]
                                encoding: NSUTF8StringEncoding];
    
    XCTAssertEqualObjects(combinedString, @"some dummy text appended",
                         @"Received data should be appended to the downloaded data");
}

@end
