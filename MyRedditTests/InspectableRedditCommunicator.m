//
//  InspectableRedditCommunicator.m
//  MyReddit
//
//  Created by Andrej Trajkovski on 13.9.17.
//  Copyright © 2017 Andrej Trajkovski. All rights reserved.
//

#import "InspectableRedditCommunicator.h"

@implementation InspectableRedditCommunicator

- (NSURL *)fetchURL {
    return fetchURL;
}

-(NSURLConnection *)fetchingConnection{
    return fetchingConnection;
}

@end
