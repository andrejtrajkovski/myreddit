

#import <XCTest/XCTest.h>
#import "Link.h"
#import "Topic.h"
#import "LinkListTableDataSource.h"
#import "LinkTableViewCell.h"
#import <OCMock/OCMock.h>
#import <UIImageView+AFNetworking.h>
#import <AFNetworking/AFImageDownloader.h>
#import <AFHTTPSessionManager.h>
#import <UIImageView+AFNetworking.h>
#import "UITableViewCell+Utility.h"

@interface LinkListTableDataSourceTests : XCTestCase

@end

@implementation LinkListTableDataSourceTests
{
    LinkListTableDataSource *dataSource;
    Topic *reddevilsTopic;
    NSIndexPath *firstCell;
    Link *link1, *link2;
    UITableView *tableView;
}

- (void)setUp {
    dataSource = [[LinkListTableDataSource alloc] init];
    tableView = [UITableView new];
    [tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"placeholder"];
    [tableView registerNib:[LinkTableViewCell nib] forCellReuseIdentifier:[LinkTableViewCell cellReuseIdentifier]];
    reddevilsTopic = [[Topic alloc] initWithName: @"reddevilsTopic"];
    dataSource.topic = reddevilsTopic;
    firstCell = [NSIndexPath indexPathForRow: 0 inSection: 0];
    link1 = [[Link alloc] init];
    link1.title = @"Link One";
    link1.score = @2;
    link1.thumbnail = [NSURL URLWithString:@""];
    link2 = [[Link alloc] init];
    link2.title = @"Link Two";
    
}
- (void)tearDown {
    
    dataSource = nil;
    tableView = nil;
    reddevilsTopic = nil;
    firstCell = nil;
    link1 = nil;
    link2 = nil;
}

- (void)testTopicWithLinksResultsInOneRowPerLinkInTheTable {
    
    [reddevilsTopic addLink: link1];
    [reddevilsTopic addLink: link2];
    XCTAssertEqual([dataSource tableView: tableView numberOfRowsInSection: 0],
                   (NSInteger)2,
                   @"Two Links in the topic means two rows in the table");
}

- (void)testContentOfPlaceholderCell {
    
    UITableViewCell *placeholderCell = [dataSource tableView: tableView cellForRowAtIndexPath: firstCell];
    XCTAssertEqualObjects(placeholderCell.textLabel.text,
                         @"There was a problem.",
                         @"The placeholder cell ought to display a placeholder message");
}

- (void)testPlaceholderCellNotReturnedWhenLinksExist {
    
    [reddevilsTopic addLink: link1];
    UITableViewCell *cell = [dataSource tableView: tableView
                            cellForRowAtIndexPath: firstCell];
    XCTAssertFalse([cell.textLabel.text isEqualToString:
                   @"There was a problem connecting to the network."], @"Placeholder should only be shown when there's no content");
}

- (void)testCellPropertiesAreTheSameAsTheLink {
    
    [reddevilsTopic addLink: link1];
    LinkTableViewCell *cell = (LinkTableViewCell *)[dataSource tableView: tableView cellForRowAtIndexPath: firstCell];
    XCTAssertEqualObjects(cell.titleLabel.text, @"Link One",
                         @"Link cells display the Link's title");
    XCTAssertEqualObjects(cell.scoreLabel.text, @"2", @"Link cells display the Link's score");
}

-(void)testCellThumbnailImageViewGetsImageItAskedFromNetwork{
    
    NSString *imageTitle = @"test";
    NSBundle *testBundle = [NSBundle bundleForClass:[self class]];
    UIImage *localImage = [UIImage imageNamed:imageTitle inBundle:testBundle compatibleWithTraitCollection:nil];
    
    NSURL *imageRemoteUrl = [NSURL URLWithString:@"http://www.color-hex.com/palettes/1051.png"];
    
    [link1 setThumbnail:imageRemoteUrl];
    [reddevilsTopic addLink: link1];
    
    AFImageResponseSerializer *mySerializer = [AFImageResponseSerializer new];
    [mySerializer setImageScale:1.0];
    [UIImageView sharedImageDownloader].sessionManager.responseSerializer = mySerializer;
    LinkTableViewCell *cell = (LinkTableViewCell *)[dataSource tableView: tableView cellForRowAtIndexPath: firstCell];
    
    XCTestExpectation *expectation = [self expectationWithDescription:@"Image expectation"];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        UIImage *thumbnailImage = cell.thumbnailImageView.image;
        XCTAssertEqualObjects(UIImagePNGRepresentation(thumbnailImage), UIImagePNGRepresentation(localImage));
        [expectation fulfill];
    });
    [self waitForExpectationsWithTimeout:5.0 handler:nil];
}

@end
