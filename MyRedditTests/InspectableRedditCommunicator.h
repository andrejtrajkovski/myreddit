//
//  InspectableRedditCommunicator.h
//  MyReddit
//
//  Created by Andrej Trajkovski on 13.9.17.
//  Copyright © 2017 Andrej Trajkovski. All rights reserved.
//

#import "RedditCommunicator.h"

@interface InspectableRedditCommunicator : RedditCommunicator

- (NSURL *)fetchURL ;
- (NSURLConnection *)fetchingConnection ;

@end
