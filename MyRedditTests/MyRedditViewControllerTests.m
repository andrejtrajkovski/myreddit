//
//  MyRedditViewControllerTests.m
//  MyRedditTests
//
//  Created by Andrej Trajkovski on 27.9.17.
//  Copyright © 2017 Andrej Trajkovski. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "MyRedditViewController.h"
#import <objc/runtime.h>
#import <OCMock/OCMock.h>
#import "TopicTableDataSource.h"
#import "TopicTableDelegate.h"
#import "Topic.h"
#import "LinkListTableDataSource.h"
#import "LinkTableViewCell.h"
#import "UITableViewCell+Utility.h"
#import "MyRedditObjectConfiguration.h"
#import "AppManager.h"

@interface MyRedditViewControllerTests : XCTestCase

@end

static const char *notificationKey = "MyRedditViewControllerTestsAssociatedNotificationKey";

@implementation MyRedditViewController (TestNotificationDelivery)
- (void)testUserDidSelectTopicNotification: (NSNotification *)note {
    objc_setAssociatedObject(self, notificationKey, note, OBJC_ASSOCIATION_RETAIN);
}

@end

static const char *viewDidAppearKey = "MyRedditViewControllerTestsViewDidAppearKey";
static const char *viewWillDisappearKey = "MyRedditViewControllerTestsViewWillDisappearKey";
static const char *viewWillAppearKey = "MyRedditViewControllerTestsViewWillAppearKey";

@implementation UIViewController (TestSuperclassCalled)

- (void)testViewDidAppear: (BOOL)animated {
    NSNumber *parameter = [NSNumber numberWithBool: animated];
    objc_setAssociatedObject(self, viewDidAppearKey, parameter, OBJC_ASSOCIATION_RETAIN);
}

- (void)testViewWillDisappear: (BOOL)animated {
    NSNumber *parameter = [NSNumber numberWithBool: animated];
    objc_setAssociatedObject(self, viewWillDisappearKey, parameter, OBJC_ASSOCIATION_RETAIN);
}

- (void)testViewWillAppear: (BOOL)animated {
    NSNumber *parameter = [NSNumber numberWithBool: animated];
    objc_setAssociatedObject(self, viewWillAppearKey, parameter, OBJC_ASSOCIATION_RETAIN);
}
@end

@implementation MyRedditViewControllerTests{
    
    MyRedditViewController *viewController;
    UITableView *tableView;
    TopicTableDataSource *tableViewDataSource;
    SEL realUserDidSelectTopicNotification;
    SEL testUserDidSelectTopicNotification;
    SEL realViewDidAppear, selectorTestViewDidAppear;
    SEL realViewWillDisappear, selectorTestViewWillDisappear;
    UINavigationController *navController;
}

- (void)setUp {
    [super setUp];
    
    viewController = [MyRedditViewController new];
    tableView = [UITableView new];
    viewController.tableView = tableView;
    tableViewDataSource = [TopicTableDataSource new];
    viewController.dataSource = tableViewDataSource;
    navController = [[UINavigationController alloc] initWithRootViewController: viewController];
    
    objc_removeAssociatedObjects(viewController);
    
    realUserDidSelectTopicNotification = @selector(userDidSelectTopicNotification:);
    testUserDidSelectTopicNotification = @selector(testUserDidSelectTopicNotification:);
    
    realViewDidAppear = @selector(viewDidAppear:);
    selectorTestViewDidAppear = @selector(testViewDidAppear:);
    
    realViewWillDisappear = @selector(viewWillDisappear:);
    selectorTestViewWillDisappear = @selector(testViewWillDisappear:);
}

- (void)tearDown {
    
    objc_removeAssociatedObjects(viewController);
    viewController = nil;
    tableView = nil;
    tableViewDataSource = nil;
    navController = nil;
    [super tearDown];
}

//+ (void)swapInstanceMethodsForClass: (Class) cls selector: (SEL)sel1 andSelector: (SEL)sel2 {
//    Method method1 = class_getInstanceMethod(cls, sel1);
//    Method method2 = class_getInstanceMethod(cls, sel2);
//    method_exchangeImplementations(method1, method2);
//}

- (void)testViewControllerHasATableViewProperty {
    objc_property_t tableViewProperty =
    class_getProperty([viewController class], "tableView");
    XCTAssertTrue(tableViewProperty != NULL,
                 @"MyRedditViewController needs a table view");
}

-(void)testViewControllerHasADataSourceProperty{
    objc_property_t dataSourceProperty =
    class_getProperty([viewController class], "dataSource");
    XCTAssertTrue(dataSourceProperty != NULL,
                  @"MyRedditViewController needs a data source");
}

- (void)testViewControllerConnectsDataSourceInViewDidLoad {
    
    [viewController viewDidLoad];
    XCTAssertEqualObjects(tableView.dataSource, tableViewDataSource);
}

- (void)testViewControllerConnectsDelegateInViewDidLoad {
    
    [viewController viewDidLoad];
    XCTAssertEqualObjects(tableView.delegate, tableViewDataSource);
}

- (void)testDefaultStateOfViewControllerDoesNotReceiveNotifications {
    [[NSNotificationCenter defaultCenter] postNotificationName: TopicTableDidSelectTopicNotification
                                                        object: nil
                                                      userInfo: nil];
    XCTAssertNil(objc_getAssociatedObject(viewController, notificationKey),
                @"Notification should not be received before -viewDidAppear:");
}

-(void)testViewControllerReceivesTableSelectionNotificationAfterViewDidAppear {
    
    id partialMockViewController = OCMPartialMock(viewController);
    OCMStub([partialMockViewController userDidSelectTopicNotification:[OCMArg any]]).andCall(viewController, testUserDidSelectTopicNotification);
    [viewController viewDidAppear: NO];
    [[NSNotificationCenter defaultCenter]
     postNotificationName: TopicTableDidSelectTopicNotification object: nil
     userInfo: nil];
    XCTAssertNotNil(objc_getAssociatedObject(viewController, notificationKey),
                   @"After -viewDidAppear: the view controller should handle" @"selection notifications");
    [partialMockViewController stopMocking];
}

- (void)testViewControllerDoesNotReceiveTableSelectNotificationAfterViewWillDisappear {
    id partialMockViewController = OCMPartialMock(viewController);
    OCMStub([partialMockViewController userDidSelectTopicNotification:[OCMArg any]]).andCall(viewController, testUserDidSelectTopicNotification);
    [viewController viewDidAppear: NO];
    [viewController viewWillDisappear: NO];
    [[NSNotificationCenter defaultCenter] postNotificationName: TopicTableDidSelectTopicNotification object: nil userInfo: nil];
    XCTAssertNil(objc_getAssociatedObject(viewController, notificationKey),
                @"After -viewWillDisappear: is called, the view controller" @"should no longer respond to topic selection notifications");
    [partialMockViewController stopMocking];
}

- (void)testViewControllerCallsSuperViewDidAppear {
    id partialMockViewController = OCMPartialMock(viewController);
    BOOL animated = YES;
    OCMStub([partialMockViewController viewDidAppear:animated]).andCall(viewController, selectorTestViewDidAppear);
    [viewController viewDidAppear: animated];
    XCTAssertNotNil(objc_getAssociatedObject(viewController, viewDidAppearKey), @"-viewDidAppear: should call through to superclass implementation");
    [partialMockViewController stopMocking];
}

- (void)testViewControllerCallsSuperViewWillDisappear {
    id partialMockViewController = OCMPartialMock(viewController);
    BOOL animated = NO;
    OCMStub([partialMockViewController viewWillDisappear:animated]).andCall(viewController, selectorTestViewWillDisappear);
    [viewController viewWillDisappear: animated];
    XCTAssertNotNil(objc_getAssociatedObject(viewController, viewWillDisappearKey), @"-viewWillDisappear: should call through to superclass implementation");
    [partialMockViewController stopMocking];
}

- (void)testSelectingTopicPushesNewViewController {
    [viewController userDidSelectTopicNotification: nil];
    UIViewController *currentTopVC = navController.topViewController;
    XCTAssertFalse([currentTopVC isEqual: viewController],@"New view controller should be pushed onto the stack");
    XCTAssertTrue([currentTopVC isKindOfClass:[MyRedditViewController class]], @"New view controller should be a MyRedditVIewController");
}

- (void)testNewViewControllerHasALinkListDataSourceForTheSelectedTopic {
    
    Topic *redDevilsTopic = [[Topic alloc] initWithName:@"reddevils"];
    NSNotification *redDevilsTopicSelectedNotification = [NSNotification notificationWithName:
                                                          TopicTableDidSelectTopicNotification object: redDevilsTopic];
    [viewController userDidSelectTopicNotification: redDevilsTopicSelectedNotification];
    MyRedditViewController *nextViewController = (MyRedditViewController *)navController.topViewController;
    XCTAssertTrue([nextViewController.dataSource isKindOfClass: [LinkListTableDataSource class]], @"Selecting a topic should push a list of links");
    XCTAssertEqualObjects([(LinkListTableDataSource *)nextViewController.dataSource topic], redDevilsTopic, @"The links to display should come from the selected topic");
}

- (void)testSelectingTopicNotificationPassesObjectConfigurationToNewViewController {
    MyRedditObjectConfiguration *objectConfiguration = [[MyRedditObjectConfiguration alloc] init];
    viewController.objectConfiguration = objectConfiguration;
    [viewController userDidSelectTopicNotification: nil];
    MyRedditViewController *newTopVC =(MyRedditViewController *)navController.topViewController;
    XCTAssertEqualObjects(newTopVC.objectConfiguration, objectConfiguration,
                         @"The object configuration should be passed through to the" @" new view controller");
}

- (void)testViewWillAppearOnLinkListInitiatesLoadingOfLinksForSelectedTopic {
    
    id mockTopic = OCMClassMock([Topic class]);
    id mockDataSource = OCMClassMock([LinkListTableDataSource class]);
    [[[mockDataSource stub] andReturn:mockTopic] topic];

    id mockAppManager = OCMClassMock([AppManager class]);
    
    id configurationMock = OCMClassMock([MyRedditObjectConfiguration class]);
    [[[configurationMock stub] andReturn:mockAppManager] appManager];
    
    [[mockAppManager expect] fetchLinksOnTopic:[OCMArg checkWithBlock:^BOOL(id obj) {
        return [obj isEqual:mockTopic];
    }]];
    
    viewController.objectConfiguration = configurationMock;
    viewController.dataSource = mockDataSource;
    [viewController viewWillAppear: YES];
    [mockAppManager verify];
}

- (void)testViewControllerConformsToAppManagerDelegateProtocol {
    XCTAssertTrue([viewController conformsToProtocol: @protocol(AppManagerDelegate)],
                 @"View controllers need to be AppManagerDelegates");
}

- (void)testViewControllerConfiguredAsAppManagerDelegateOnManagerCreation {
    
    AppManager *mockAppManager = [AppManager new];
    id configurationMock = OCMClassMock([MyRedditObjectConfiguration class]);
    [[[configurationMock stub] andReturn:mockAppManager] appManager];
    viewController.objectConfiguration = configurationMock;
    [viewController viewWillAppear: YES];
    XCTAssertEqualObjects(viewController.manager.delegate, viewController,
                          @"View controller sets itself as the manager's delegate");
}

- (void)testDownloadedQuestionsAreAddedToTopic {
    LinkListTableDataSource *topicDataSource = [[LinkListTableDataSource alloc] init];
    viewController.dataSource = topicDataSource;
    Topic *topic = [[Topic alloc] initWithName: @"reddevils"];
    topicDataSource.topic = topic;
    Link *link1 = [[Link alloc] init];
    [viewController didReceiveLinks:[NSArray arrayWithObject: link1]];
    XCTAssertEqualObjects([topic.links lastObject], link1, @"Link was added to the topic");
}

- (void)testTableViewReloadedWhenQuestionsReceived {
    id mockTableView = OCMPartialMock(viewController.tableView);
    [[mockTableView expect] reloadData];
    [viewController didReceiveLinks: [NSArray array]];
    [mockTableView verify];
}

@end
