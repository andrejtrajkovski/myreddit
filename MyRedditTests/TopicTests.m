//
//  TopicTests.m
//  MyReddit
//
//  Created by Andrej Trajkovski on 08.9.15.
//  Copyright (c) 2015 г. Andrej Trajkovski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "Topic.h"
#import "Link.h"

@interface TopicTests : XCTestCase

@end

@implementation TopicTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

-(void)testThatTopicExists{
    
    Topic *newTopic = [[Topic alloc] init];
    XCTAssertNotNil(newTopic,@"should be able to create a Topic instance");
}

-(void)testThatTopicCanBeNamed{
    
    static NSString *topicName = @"iOS Programming";
    Topic *namedTopic = [[Topic alloc] initWithName:topicName];
    XCTAssertEqual(topicName, namedTopic.name, @"the Topic should have the name I gave it");
}

- (void)testForInitiallyEmptylinksList {
    Topic *topic = [[Topic alloc] init];
    XCTAssertEqual([[topic links] count], (NSUInteger)0,
                   @"No links added yet, count should be zero");
}

- (void)testAddinglinksToTheTopic {
    static NSString *topicName = @"iOS Programming";
    Topic *topic = [[Topic alloc] initWithName:topicName];
    Link *newLink = [Link new];
    [topic addLink: newLink];
    XCTAssertEqual([[topic links] count], (NSUInteger)1,
                   @"Add a link, and the count of links should go up");
}

@end
