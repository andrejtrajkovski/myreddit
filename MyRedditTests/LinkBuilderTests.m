//
//  LinkBuilderTests.m
//  MyReddit
//
//  Created by Andrej Trajkovski on 25.2.16.
//  Copyright © 2016 Andrej Trajkovski. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "LinkBuilder.h"
#import "Link.h"

@interface LinkBuilderTests : XCTestCase

@end

@implementation LinkBuilderTests{

    LinkBuilder *linkBuilder;
}

- (void)setUp {
    [super setUp];

    linkBuilder = [LinkBuilder new];
}

- (void)tearDown {
    
    linkBuilder = nil;
    [super tearDown];
}

-(void)testThatNilIsNotAnAcceptableParameter{
    XCTAssertThrows([linkBuilder linksFromJSON:nil error:NULL], @"Lack of data should be handled elsewhere");
}

- (void)testNilReturnedWhenStringIsNotJSON {
    XCTAssertNil([linkBuilder linksFromJSON: @"Not JSON"
                                             error: NULL],
                @"This parameter should not be parsable");
}
- (void)testErrorSetWhenStringIsNotJSON {
    NSError *error = nil;
    [linkBuilder linksFromJSON:@"Not JSON" error: &error];
    XCTAssertNotNil(error, @"An error occurred, we should be told");
}
- (void)testPassingNullErrorDoesNotCauseCrash {
    XCTAssertNoThrow([linkBuilder linksFromJSON: @"Not JSON"
                                                 error: NULL],
                    @"Using a NULL error parameter should not be a problem");
}

-(void)testKindNotListingsReturnsMissingDataError{
    
    NSError *error = [NSError errorWithDomain:@"LinkBuilderErrorDomain" code:LinkBuilderMissingDataError userInfo:nil];
    NSDictionary *JSONDic= @{@"kind" : @"NotAListing"};
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:JSONDic
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSError *error2;
    [linkBuilder linksFromJSON:jsonString error:&error2];
    XCTAssertEqualObjects(error, error2);
}

-(void)testStringThatHasNoDataDictionaryReturnsMissingDataError{
    
    NSError *error = [NSError errorWithDomain:@"LinkBuilderErrorDomain" code:LinkBuilderMissingDataError userInfo:nil];
    NSDictionary *JSONDic= @{@"kind" : @"Listing",
                             @"data" : @[]};
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:JSONDic
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSError *error2;
    [linkBuilder linksFromJSON:jsonString error:&error2];
    XCTAssertEqualObjects(error, error2);
}

- (void)testLinkCreatedFromJSONHasPropertiesPresentedInJSON {
    
    NSString *firstLinkDomain = @"twitter.com";
    NSString *firstLinkId = @"5rnkmi";
    NSString *firstLinkTitle = @"Valencia chosen as player of the month for January";
    NSString *firstLinkUrl = @"https://twitter.com/ManUtd/status/827125689711210496";
    NSNumber *firstLinkScore = [NSNumber numberWithInteger:127];
    NSError *dummyError;
    
    NSDictionary *JSONDic=
                        @{@"kind" : @"Listing",
                         @"data" : @{
                                 @"children" :@[
                                         @{@"kind" : @"t3",
                                           @"data" : @{
                                                   @"domain" : firstLinkDomain,
                                                   @"id" : firstLinkId,
                                                   @"title" : firstLinkTitle,
                                                   @"url" : firstLinkUrl,
                                                   @"score" : firstLinkScore
                                                   }
                                           }
                                         ]
                                 }
                         };
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:JSONDic
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    Link *linkFromBuilder = [[linkBuilder linksFromJSON:jsonString error:&dummyError] firstObject];
    
    XCTAssertEqualObjects(linkFromBuilder.identifier, firstLinkId);
    XCTAssertEqualObjects(linkFromBuilder.score, firstLinkScore);
    XCTAssertEqualObjects(linkFromBuilder.title, firstLinkTitle);
    XCTAssertEqualObjects(linkFromBuilder.urlString, firstLinkUrl);
    XCTAssertEqualObjects(linkFromBuilder.domain, firstLinkDomain);
    
}


@end
