//
//  MockAppManagerDelegate.h
//  MyReddit
//
//  Created by Andrej Trajkovski on 08.11.16.
//  Copyright © 2016 Andrej Trajkovski. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppManagerDelegate.h"

@interface MockAppManagerDelegate : NSObject <AppManagerDelegate>

@property (strong, nonatomic) NSError *fetchError;
-(void)fetchingLinksFailedWithError:(NSError *)error;
-(void)didReceiveLinks:(NSArray *)links;

@end
