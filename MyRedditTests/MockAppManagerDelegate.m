//
//  MockAppManagerDelegate.m
//  MyReddit
//
//  Created by Andrej Trajkovski on 08.11.16.
//  Copyright © 2016 Andrej Trajkovski. All rights reserved.
//

#import "MockAppManagerDelegate.h"

@implementation MockAppManagerDelegate

-(void)fetchingLinksFailedWithError:(NSError *)error{
    
    self.fetchError = error;
}

@end
