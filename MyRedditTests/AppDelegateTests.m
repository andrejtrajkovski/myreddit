//
//  AppDelegateTests.m
//  MyRedditTests
//
//  Created by Andrej Trajkovski on 27.11.17.
//  Copyright © 2017 Andrej Trajkovski. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "AppDelegate.h"
#import "MyRedditViewController.h"
#import "TopicTableDataSource.h"
#import "Topic.h"

@interface AppDelegateTests : XCTestCase

@end

@implementation AppDelegateTests{
    
    UIWindow *window;
    AppDelegate *appDelegate;
}

- (void)setUp {
    [super setUp];
    window = [[UIWindow alloc] init];
    appDelegate = [[AppDelegate alloc] init];
    appDelegate.window = window;
//    didFinishLaunchingWithOptionsReturn = [appDelegate application: nil didFinishLaunchingWithOptions: nil];
}

- (void)tearDown {
    window = nil;
    appDelegate = nil;
    [super tearDown];
}

- (void)testWindowRootControllerIsNavigationController {
    [appDelegate application: nil didFinishLaunchingWithOptions: nil];
    id windowRootViewController = appDelegate.window.rootViewController;
    XCTAssertTrue([windowRootViewController isKindOfClass: [UINavigationController class]],
                  @"Views in this app are supplied by MyRedditViewControllers");
}

- (void)testNavigationControllerShowsABrowseOverflowViewController {
    [appDelegate application: nil didFinishLaunchingWithOptions: nil];
    UINavigationController *windowRootViewController = appDelegate.window.rootViewController;
    id visibleViewController = windowRootViewController.topViewController;
    XCTAssertTrue([visibleViewController isKindOfClass: [MyRedditViewController class]],
                  @"Views in this app are supplied by MyRedditViewControllers");
}

- (void)testFirstViewControllerHasATopicTableDataSource {
    [appDelegate application: nil didFinishLaunchingWithOptions: nil];
    UINavigationController *windowRootViewController = appDelegate.window.rootViewController;
    MyRedditViewController *visibleViewController = windowRootViewController.topViewController;
    XCTAssertTrue([visibleViewController.dataSource isKindOfClass: [TopicTableDataSource class]],
                 @"First view should display a list of topics");
}

- (void)testTopicListIsCorrect {
    
    NSString *topicNames[] = { @"iOSProgramming", @"objectiveC", @"redDevils", @"worldNews", @"unitTesting" };
    
    [appDelegate application: nil didFinishLaunchingWithOptions: nil];
    TopicTableDataSource *dataSource = [(MyRedditViewController *)[(UINavigationController *)appDelegate.window.rootViewController topViewController] dataSource];
    for (int i = 0; i < 5; i++) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
        XCTAssertEqualObjects([[dataSource topicForIndexPath:indexPath] name], topicNames[i]);
    }
}

- (void)testFirstViewControllerHasAnObjectConfiguration {
    [appDelegate application: nil didFinishLaunchingWithOptions: nil];
    MyRedditViewController *topicViewController = (MyRedditViewController *)[(UINavigationController *)appDelegate.window.rootViewController topViewController];
    XCTAssertNotNil(topicViewController.objectConfiguration, @"The view controller should have an object configuration instance");
}

@end
