//
//  TopicTableDataSourceTests.m
//  MyRedditTests
//
//  Created by Andrej Trajkovski on 20.10.17.
//  Copyright © 2017 Andrej Trajkovski. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "TopicTableDataSource.h"
#import "Topic.h"
#import <OCMock/OCMock.h>

@interface TopicTableDataSourceTests : XCTestCase

@end

@implementation TopicTableDataSourceTests{
    
    TopicTableDataSource *dataSource;
    NSArray *topicsList;
}

- (void)setUp {
    [super setUp];
    dataSource = [TopicTableDataSource new];
    Topic *sampleTopic = [[Topic alloc] initWithName: @"reddevils"];
    topicsList = [NSArray arrayWithObject: sampleTopic];
    [dataSource setTopics:topicsList];
}

- (void)tearDown {
    
    dataSource = nil;
    [super tearDown];
}

- (void)testTopicDataSourceCanReceiveAListOfTopics {
    XCTAssertNoThrow([dataSource setTopics: topicsList],
                    @"The data source needs a list of topics");
}

-(void)testOneTableRowForOneTopic{
    XCTAssertEqual([dataSource tableView:[OCMArg any] numberOfRowsInSection:0], [topicsList count]);
}

- (void)testTwoTableRowsForTwoTopics {
    Topic *sampleTopic = [[Topic alloc] initWithName: @"reddevils"];
    Topic *sampleTopic2 = [[Topic alloc] initWithName: @"soccer"];
    NSArray *twoTopicsList = [NSArray arrayWithObjects:
                              sampleTopic, sampleTopic2, nil];
    [dataSource setTopics: twoTopicsList];
    NSInteger numberOfRowsResult = [dataSource tableView: [OCMArg any] numberOfRowsInSection: 0];
    XCTAssertEqual((NSInteger)[twoTopicsList count], numberOfRowsResult,
                   @"There should be two rows in the table for two topics");
}

- (void)testOneSectionInTheTableView {
    XCTAssertThrows([dataSource tableView: [OCMArg any]
                   numberOfRowsInSection: 1],
                   @"Data source doesn't allow asking about additional sections");
}

- (void)testDataSourceCellCreationExpectsOneSection {
    NSIndexPath *secondSection = [NSIndexPath indexPathForRow: 0
                                                    inSection: 1];
    XCTAssertThrows([dataSource tableView: [OCMArg any]
                   cellForRowAtIndexPath: secondSection],
                   @"Data source will not prepare cells for unexpected sections");
}

- (void)testDataSourceCellCreationWillNotCreateMoreRowsThanItHasTopics{
    NSIndexPath *afterLastTopic =
    [NSIndexPath indexPathForRow: [topicsList count] inSection: 0];
    XCTAssertThrows([dataSource tableView: [OCMArg any]
                   cellForRowAtIndexPath: afterLastTopic],
                   @"Data source will not prepare more cells than there are topics");
}

- (void)testCellCreatedByDataSourceContainsTopicTitleAsTextLabel {
    NSIndexPath *firstTopic = [NSIndexPath indexPathForRow: 0
                                                 inSection: 0];
    
    id tableMock = [OCMockObject niceMockForClass:[UITableView class]];
    
    UITableViewCell *firstCell = [dataSource tableView: tableMock
                                 cellForRowAtIndexPath: firstTopic];
    NSString *cellTitle = firstCell.textLabel.text;
    XCTAssertEqualObjects(@"reddevils", cellTitle,
                         @"Cell's title should be equal to the topic's title");
}

- (void)testDataSourceIndicatesWhichTopicIsRepresentedForAnIndexPath {
    NSIndexPath *firstRow = [NSIndexPath indexPathForRow: 0 inSection: 0];
    Topic *firstTopic = [dataSource topicForIndexPath: firstRow];
    XCTAssertEqualObjects(firstTopic.name, @"reddevils", @"The iPhone Topic is at row 0");
}

@end
