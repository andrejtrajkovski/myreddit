//
//  PostTests.m
//  MyReddit
//
//  Created by Andrej Trajkovski on 06.10.15.
//  Copyright © 2015 г. Andrej Trajkovski. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "Link.h"
#import "Comment.h"
@interface LinkTests : XCTestCase

@end

@implementation LinkTests{

    Link *myLink;
}

- (void)setUp {
    [super setUp];
    
    myLink = [[Link alloc] init];
    myLink.title = @"My First Post";
    myLink.comments = @[];
    myLink.domain = @"youtube.com";
    myLink.urlString = @"https://www.youtube.com/watch?v=9-NFosnfd2c";
    myLink.score = @30;
}

- (void)tearDown {
    
    myLink = nil;
    [super tearDown];
}

-(void) testLinkIsOfPostTypeLink{

    XCTAssertEqual(myLink.postType, PostTypeLink, @"Link should be of post type link.");
}

- (void) testLinkHasATitle{

    XCTAssertEqualObjects(@"My First Post", myLink.title, @"Wrong title.");
}

- (void) testLinkHasAnIdentifier{
    
    myLink.identifier = @"1000";
    XCTAssertEqualObjects(@"1000", myLink.identifier, @"Wrong id.");
}

-(void)testPostDomain{
    
    XCTAssertEqualObjects(@"youtube.com", myLink.domain,@"Wrong domain.");
}

- (void) testLinkHasAURLString{
    
    XCTAssertEqualObjects(@"https://www.youtube.com/watch?v=9-NFosnfd2c", myLink.urlString, @"Wrong url.");
}

-(void)testLinkHasSomeComments{

    XCTAssertNotNil(myLink.comments, @"Post has no comments.");
}

-(void)testLinkScore{

    XCTAssertEqualObjects(myLink.score, @30);
}

-(void)testCommentsAreArray{
    
    XCTAssertTrue([myLink.comments isKindOfClass:[NSArray class]]);
}

-(void) testAllCommentsArrayElementsAreComments{
    
    for(id comment in myLink.comments){
    
        XCTAssertTrue([comment isMemberOfClass:[Comment class]]);
    }
}

@end
