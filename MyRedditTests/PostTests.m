//
//  PostTests.m
//  MyReddit
//
//  Created by Andrej Trajkovski on 11.10.15.
//  Copyright © 2015 г. Andrej Trajkovski. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "Post.h"
#import "Comment.h"
@interface PostTests : XCTestCase

@end

@implementation PostTests{

    Post *myPost;
}

- (void)setUp {
    [super setUp];
    myPost = [Post new];
    myPost.title = @"My Title";
    myPost.comments = @[];
    myPost.score = @25;
    myPost.identifier = @"100";
    myPost.author = @"Andrej";
}

- (void) testPostHasAIdentifier{
    
    XCTAssertEqualObjects(@"100", myPost.identifier, @"Wrong Id.");
}

- (void) testPostHasATitle{
    
    XCTAssertEqualObjects(@"My Title", myPost.title, @"Wrong title.");
}

- (void) testPostHasAnAuthor{
    
    XCTAssertEqualObjects(@"Andrej", myPost.author, @"Wrong author.");
}

-(void)testPostHasSomeComments{
    
    XCTAssertNotNil(myPost.comments, @"Post has no comments.");
}

-(void)testPostScore{
    
    XCTAssertEqualObjects(myPost.score, @25);
}

-(void)testCommentsAreArray{
    
    XCTAssertTrue([myPost.comments isKindOfClass:[NSArray class]]);
}

-(void) testAllCommentsArrayElementsAreComments{
    
    Comment *commentOne = [Comment new];
    myPost.comments = @[commentOne];
    
    for(id comment in myPost.comments){
        BOOL isObjectInstanceOfComment = [comment isMemberOfClass:[Comment class]];
        XCTAssertTrue(isObjectInstanceOfComment);
    }
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    // Use recording to get started writing UI tests.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
}

@end
