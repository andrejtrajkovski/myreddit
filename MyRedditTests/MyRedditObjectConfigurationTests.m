//
//  MyRedditObjectConfigurationTests.m
//  MyRedditTests
//
//  Created by Andrej Trajkovski on 28.11.17.
//  Copyright © 2017 Andrej Trajkovski. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "MyRedditObjectConfiguration.h"
#import "AppManager.h"

@interface MyRedditObjectConfigurationTests : XCTestCase

@end

@implementation MyRedditObjectConfigurationTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}


- (void)testConfigurationOfCreatedAppManager {
    MyRedditObjectConfiguration *configuration =
    [[MyRedditObjectConfiguration alloc] init];
    AppManager *manager = [configuration appManager];
    XCTAssertNotNil(manager, @"The AppManager should exist");
    XCTAssertNotNil(manager.communicator, @"Manager should have a RedditCommunicator");
    XCTAssertNotNil(manager.linkBuilder, @"Manager should have a link builder");
    XCTAssertNotNil(manager.commentBuilder, @"Manager should have an comment builder");
    XCTAssertEqualObjects(manager.communicator.delegate, manager, @"The manager is the communicator’s delegate");
}

@end
