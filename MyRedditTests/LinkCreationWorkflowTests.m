//
//  LinkCreationTests.m
//  MyReddit
//
//  Created by Andrej Trajkovski on 01.12.15.
//  Copyright © 2015 Andrej Trajkovski. All rights reserved.
//

#import "APPManagerDelegate.h"
#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>
#import "AppManager.h"
#import "RedditCommunicator.h"
#import "Link.h"
#import "MockAppManagerDelegate.h"

@interface LinkCreationWorkflowTests : XCTestCase
@end

@implementation LinkCreationWorkflowTests{

    NSError *underlyingError;
    AppManager *mgr;
    id ocMockAppManagerDelegate;
    id linkBuilderMock;
    NSArray *linkArray;
}

- (void)setUp{
    [super setUp];
    mgr = [AppManager new];
    
    ocMockAppManagerDelegate = [OCMockObject mockForProtocol:@protocol(AppManagerDelegate)];
    mgr.delegate = ocMockAppManagerDelegate;
    
    linkBuilderMock = OCMClassMock([LinkBuilder class]);
    mgr.linkBuilder = linkBuilderMock;

    underlyingError = [NSError errorWithDomain: @"Test domain"
                                           code: 0 userInfo: nil];
    
    id link = [OCMockObject mockForClass:[Link class]];
    linkArray = [NSArray arrayWithObject:link];
}

- (void)tearDown{
    mgr = nil;
    ocMockAppManagerDelegate = nil;
    underlyingError = nil;
    linkArray = nil;
    linkBuilderMock = nil;
    [super tearDown];
}

-(void)testNonConformingObjectsCannotBeDelegate{
    
    XCTAssertThrows(mgr.delegate = (id <AppManagerDelegate>)[NSNull null],
                    @"NSNull should not be used as the delegate as it doesn't conform to the delegate protocol");
}

-(void)testConformingObjectsCanBeDelegate{

    XCTAssertNoThrow(mgr.delegate,
                     @"Object conforming to the delegate protocol should be used as the delegate.");
}

- (void)testManagerAcceptsNilAsADelegate{

    XCTAssertNoThrow(mgr.delegate = nil,
                    @"It should be acceptable to use nil as an object’s delegate");
}

-(void)testFetchingLinksMeansRequestingJSON{
    Topic *newTopic = [[Topic alloc] initWithName:@"reddevils"];
    id mockCommunicator = OCMClassMock([RedditCommunicator class]);
    mgr.communicator = mockCommunicator;
    [mgr fetchLinksOnTopic:newTopic];
    OCMVerify([mockCommunicator searchForLinksOnTopic:newTopic]);
}

-(void)testErrorReturnedToDelegateIsNotErrorNotifiedByCommunicatorStateBehavior{
    
    MockAppManagerDelegate *delegate = [MockAppManagerDelegate new];
    mgr.delegate = delegate;

    [mgr searchingForLinksFailedWithError: underlyingError];
    
    XCTAssertNotEqualObjects([delegate fetchError], underlyingError, @"Error should be at the correct level of abstraction");
}

-(void)testErrorReturnedToDelegateIsNotErrorNotifiedByCommunicator{
    
    [[ocMockAppManagerDelegate expect] fetchingLinksFailedWithError:[OCMArg checkWithBlock:^BOOL(id value){
        return ![underlyingError isEqual:value];
    }]];
    [mgr searchingForLinksFailedWithError:underlyingError];
    OCMVerify(ocMockAppManagerDelegate);
}

- (void)testErrorReturnedToDelegateDocumentsUnderlyingError {
    [[ocMockAppManagerDelegate expect] fetchingLinksFailedWithError:[OCMArg checkWithBlock:^BOOL(NSError *reportableError){
        NSLog(@"rep error underlying %@",[[[reportableError userInfo] objectForKey:NSUnderlyingErrorKey] domain]);
        return [underlyingError isEqual:[[reportableError userInfo] objectForKey:NSUnderlyingErrorKey]];
    }]];
    [mgr searchingForLinksFailedWithError: underlyingError];
    OCMVerifyAll(ocMockAppManagerDelegate);
}

- (void)testLinkJSONIsPassedToLinkBuilder {

    id niceOcMockAppManagerDelegate = [OCMockObject niceMockForProtocol:@protocol(AppManagerDelegate)];
    mgr.delegate = niceOcMockAppManagerDelegate;
    NSString *fakeJSONString = @"Fake JSON";
    NSError *dummyError = [NSError errorWithDomain: @"Dummy error"
                                              code: 0 userInfo: nil];
    [[linkBuilderMock expect] linksFromJSON:[OCMArg checkWithBlock:^BOOL(NSString *jsonSentToBuilder){
        
        return [jsonSentToBuilder isEqual:fakeJSONString];
        
    }] error:[OCMArg setTo:dummyError]];
    
    [mgr receivedLinksJSON:fakeJSONString];

    OCMVerifyAll(linkBuilderMock);
}

- (void)testDelegateNotifiedOfErrorWhenLinkBuilderFails {
    
    NSError *dummyError = [NSError errorWithDomain: @"AppManagerError" code: 0 userInfo: nil];
    
    [[linkBuilderMock expect] linksFromJSON:@"Fake JSON" error: [OCMArg setTo:dummyError]];
    
    [[ocMockAppManagerDelegate expect] fetchingLinksFailedWithError:[OCMArg checkWithBlock:^BOOL(NSError *error) {
        
        return [[error userInfo] objectForKey:NSUnderlyingErrorKey] == dummyError;
        
    }]];
    
    [mgr receivedLinksJSON:@"Fake JSON"];
    
    OCMVerifyAll(ocMockAppManagerDelegate);
}

- (void)testDelegateNotToldAboutErrorWhenLinksReceived {
    
    id niceOcMockAppManagerDelegate = [OCMockObject niceMockForProtocol:@protocol(AppManagerDelegate)];
    [[niceOcMockAppManagerDelegate reject] fetchingLinksFailedWithError:[OCMArg any]];
    
    mgr.delegate = niceOcMockAppManagerDelegate;
    
    OCMStub([linkBuilderMock linksFromJSON:[OCMArg any] error:[OCMArg setTo:nil]]).andReturn(linkArray);
    [mgr receivedLinksJSON:[OCMArg any]];
    
    OCMVerifyAll(niceOcMockAppManagerDelegate);
}

- (void)testDelegateReceivesTheLinksDiscoveredByManager {
    
    NSError *dummyError = [NSError errorWithDomain: @"AppManagerError" code: 0 userInfo: nil];
    id mockAppManagerDelegate = [OCMockObject mockForProtocol:@protocol(AppManagerDelegate)];
    [[mockAppManagerDelegate expect] didReceiveLinks:linkArray];
    mgr.delegate = mockAppManagerDelegate;
    
    OCMStub([linkBuilderMock linksFromJSON:[OCMArg isNotNil] error:[OCMArg setTo:dummyError]]).andReturn(linkArray);
    [mgr receivedLinksJSON:[OCMArg any]];
    
    OCMVerifyAll(mockAppManagerDelegate);
}

@end
