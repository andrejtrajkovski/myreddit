//
//  CommentTests.m
//  MyReddit
//
//  Created by Andrej Trajkovski on 11.10.15.
//  Copyright © 2015 г. Andrej Trajkovski. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "Comment.h"

@interface CommentTests : XCTestCase

@end

@implementation CommentTests{
    
    Comment *myComment;
}

- (void)setUp {
    [super setUp];
    myComment = [Comment new];
    myComment.identifier = @"10";
    myComment.comments = @[];
}

- (void)tearDown {

    myComment = nil;
    [super tearDown];
}

-(void) testLinkIsOfPostTypeLink{
    
    XCTAssertEqual(myComment.postType, PostTypeComment, @"Comment should be of post type comment.");
}

-(void) testAllCommentsArrayElementsAreComments{
    
    for(id comment in myComment.comments){
        
        XCTAssertTrue([comment isMemberOfClass:[Comment class]]);
    }
}

-(void)testCommentHasAnId{

    XCTAssertNotNil(myComment.identifier, @"This comment has no id.");
}

@end
